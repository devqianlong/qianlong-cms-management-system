<!--
 * @Description    : 说明
 * @Author         : QianLong
 * @Date           : 2022-07-01 15:44:45
 * @LastEditors    : QianLong
 * @Site           : http://21ds.cn
 * @LastEditTime   : 2023-07-07 18:02:27
-->
# 千龙cms免费开源版

#### 介绍
千龙Cms 是由 千龙 主导开发的前后分离的管理系统，使用最新的ant design vue v4做为前端框架，内置了动态路由，权限验证，微信/支付宝聚合支付、Email、旋转验证码、扫码授权登录、角色管理、文章管理、OSS存储（对接开发者平台，免费）、短信功能（对接开发者平台，免费）、邮件功能（对接开发者平台，免费）、G2Plot图表、广告管理等功能，同时，实现了按钮级权限控制，token无感刷新等功能，免费版有问题可直接提issue

#### 软件架构

程序基于ThinkPHP6 + Antdv4 + Vue3 + Vite开发

#### 软件运行环境：

PHP7以上、Mysql、Redis、Linux、composer

#### 使用说明：

数据库：qianlong_cms_os.sql，将数据库导入至mysql

Thinkphp6安装：composer install，修改数据库连接：tp/.env

前端代码运行：

修改域名为你自己的域名：vue/src/utils/common.ts，修改 http://cms.demo.cn/ 为你自己的域名
前端安装：yarn install，开发运行：yarn run serve，前端打包：yarn run build

管理中心登录账号密码：dragon，dragon123654

有问题可加群咨询：
![输入图片说明](21.png)