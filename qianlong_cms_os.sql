-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_ads`
--

DROP TABLE IF EXISTS `ql_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_ads` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cate_id` int unsigned DEFAULT NULL,
  `create_time` int unsigned DEFAULT NULL,
  `sort` int unsigned DEFAULT NULL,
  `status` tinyint unsigned DEFAULT '1',
  `start_time` int unsigned DEFAULT NULL,
  `end_time` int unsigned DEFAULT NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='广告列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_ads`
--

/*!40000 ALTER TABLE `ql_ads` DISABLE KEYS */;
/*!40000 ALTER TABLE `ql_ads` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:20
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_ads_cate`
--

DROP TABLE IF EXISTS `ql_ads_cate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_ads_cate` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '状态',
  `sort` mediumint DEFAULT '0' COMMENT '排序',
  `pid` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='广告分类';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_ads_cate`
--

/*!40000 ALTER TABLE `ql_ads_cate` DISABLE KEYS */;
INSERT INTO `ql_ads_cate` VALUES (1,'测试分类',1,10,0);
/*!40000 ALTER TABLE `ql_ads_cate` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:21
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_article`
--

DROP TABLE IF EXISTS `ql_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_article` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `status` tinyint DEFAULT '1',
  `create_time` int DEFAULT NULL,
  `cate_id` int DEFAULT NULL COMMENT '分类ID',
  `author_id` int DEFAULT NULL COMMENT '发布者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文章';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_article`
--

/*!40000 ALTER TABLE `ql_article` DISABLE KEYS */;
INSERT INTO `ql_article` VALUES (1,'这是标题','<pre><code class=\"C++\">#include &lt;iostream&gt;\n\nint main(){\n}</code></pre>',1,1636961509,1,NULL);
/*!40000 ALTER TABLE `ql_article` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:22
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_article_cate`
--

DROP TABLE IF EXISTS `ql_article_cate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_article_cate` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '状态',
  `sort` mediumint DEFAULT '0' COMMENT '排序',
  `pid` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_article_cate`
--

/*!40000 ALTER TABLE `ql_article_cate` DISABLE KEYS */;
INSERT INTO `ql_article_cate` VALUES (1,'测试分类',1,0,0);
/*!40000 ALTER TABLE `ql_article_cate` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:23
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_auth_group`
--

DROP TABLE IF EXISTS `ql_auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `status` smallint DEFAULT NULL COMMENT '状态',
  `mid` int DEFAULT NULL COMMENT '主管理ID',
  `updateTime` int DEFAULT NULL COMMENT '更新时间',
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '所有权限列表',
  `desc` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `halfrules` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '半选中权限列表',
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_auth_group`
--

/*!40000 ALTER TABLE `ql_auth_group` DISABLE KEYS */;
INSERT INTO `ql_auth_group` VALUES (1,'超级管理员',1,1,1620356463,'7,8,1,2,5,4,21,23,22,3,6,24,25,26,27,28,29,30,49,32,33,34,36,37,31,48,50,51,9,38,10,19,20,35,11,13,12,18,14,16,17,45,43,44,46,39,40,42,47,41,52,53,54,55,56,58,57','全部权限',''),(2,'次管理员',1,1,1653830750,'7,8,5,10,19,20,35,11,13,12,18,14,16,17,52','测试','2,1,9,47,39');
/*!40000 ALTER TABLE `ql_auth_group` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:24
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_auth_menu`
--

DROP TABLE IF EXISTS `ql_auth_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_auth_menu` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pid` int DEFAULT '0' COMMENT '上级ID',
  `key` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '唯一标识',
  `component` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组件',
  `status` tinyint DEFAULT '0' COMMENT '1=正常，-1=禁用,2=开发版，3=内测版',
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '标题',
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图标',
  `redirect` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '重定向',
  `type` tinyint DEFAULT NULL COMMENT '1=目录，2=菜单，3=操作',
  `sort` mediumint DEFAULT NULL,
  `actionUrl` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '接口API地址',
  `filePath` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '页面路径',
  `hideChildrenInMenu` tinyint(1) DEFAULT '0',
  `hidden` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`pid`) USING BTREE,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_auth_menu`
--

/*!40000 ALTER TABLE `ql_auth_menu` DISABLE KEYS */;
INSERT INTO `ql_auth_menu` VALUES (1,0,'article','PageView',1,'文章管理','el_wenzhangguanli','',1,20,'','',0,0),(2,1,'ArticleList','',1,'文章列表','el_wenzhangguanli1','',2,20,'api/article/list','',1,0),(3,1,'ArticleCate','',1,'分类管理','el_wenzhangfenlei1','',2,30,'api/article/cate_list','',1,0),(4,2,'ItemEdit','',1,'文章编辑','','',2,10,'api/article/edititeminfo','',1,1),(5,2,'article_edit','',1,'新增文章','','',2,5,'api/article/edititeminfo','ItemEdit',1,1),(6,3,'article_cate_edit','',1,'编辑','','',2,10,'api/article/editcateitem','ItemEdit',0,1),(7,0,'dashboard','RouteView',1,'仪表盘','el_yibiaopan','/dashboard/workplace',1,10,'','',0,0),(8,7,'Workplace','',1,'工作台','el_gongzuotai','',2,10,'','',0,0),(9,0,'system','PageView',1,'系统管理','el_guanli','',2,9000,'','0',0,0),(10,9,'UserList','',1,'人员管理','el_kehuguanli','',2,10,'api/user/getuserlist','',1,0),(11,9,'RoleList','',1,'角色管理','el_jiaosequanxian','',2,20,'api/roles/index','0',0,0),(12,11,'role_edit',NULL,1,'编辑','',NULL,3,10,'api/roles/editauthgroup','0',0,0),(13,11,'role_add',NULL,1,'新增','',NULL,3,5,'api/roles/addauthgroup','0',0,0),(14,18,'role_del','',1,'删除','','',3,15,'api/roles/delauthgroup','0',0,0),(15,18,'role_view','',2,'详情','','',3,20,'api/roles/getgroupinfo','0',0,0),(16,18,'role_fbid','',1,'禁用/解禁','','',3,30,'api/roles/forbidauthgroup','0',0,0),(17,11,'role_search',NULL,1,'搜索',NULL,NULL,3,40,'api/roles/index','0',0,0),(18,11,'role_ext','',1,'扩展操作','','',3,10,'','0',0,0),(19,10,'user_search','',1,'搜索','','',3,10,'api/user/getuserlist','0',0,0),(20,10,'user_edit','',1,'编辑','','',2,20,'api/user/edituserinfo','ItemEdit',0,1),(21,2,'article_search','',1,'搜索','','',3,10,'api/article/list','',0,0),(22,23,'article_del','',1,'删除','','',3,40,'api/article/delart','',0,0),(23,2,'article_ext','',1,'更多操作','','',3,10,'api/article/list','',0,0),(24,3,'article_cate_search','',1,'搜索','','',3,20,'api/article/cate_list','',0,0),(25,3,'article_cate_add','',1,'新增分类','','',3,40,'api/article/editcateitem','ItemEdit',0,0),(26,3,'article_cate_ext','',1,'更多操作','','',3,50,'','',0,0),(27,26,'article_cate_del','',1,'删除','','',3,70,'api/article/delcate','',0,0),(28,0,'adsManage','PageView',1,'广告管理','el_guanggaoguanli','',1,40,'api/ads/index','',0,0),(29,28,'AdsList','',1,'广告列表','el_guanggaoguanli1','',2,20,'api/ads/list','List',1,0),(30,29,'ads_search','',1,'搜索','','',3,10,'api/ads/list','',0,0),(31,36,'ads_cate_add','',1,'分类新增','','',2,20,'api/ads/add_cate','ItemEdit',0,1),(32,29,'ads_edit','',1,'编辑广告','','',2,30,'api/ads/edit','ItemEdit',0,1),(33,29,'ads_ext','',1,'更多操作','','',3,40,'','',0,0),(34,33,'ads_del','',1,'删除','','',3,50,'api/ads/del','',0,0),(35,10,'user_add','',1,'新增人员','','',2,40,'api/user/edituserinfo','',0,1),(36,28,'AdsCate','',1,'广告分类','el_guanggaofenleiguanli','',2,40,'api/ads/cate','Cate',1,0),(37,36,'ads_cate_search','',1,'搜索','','',3,10,'api/ads/cate_list','',0,0),(38,7,'Analysis','',1,'分析页','el_fenxi','',2,20,'api/system/setting','',1,0),(39,9,'MenuList','',1,'菜单管理','el_menus','',2,6000,'api/menu/index','',0,0),(40,39,'menu_add','',1,'新增/编辑菜单','','',3,10,'api/menu_edit','',1,1),(41,47,'menu_del','',1,'菜单删除','','',3,10,'api/menu/menu_del','',1,1),(42,39,'menu_search','',1,'搜索','','',3,20,'api/menu/auth_menu','',1,1),(43,9,'SmsSetting','',1,'短信设置','el_duanxinshezhi','',2,50,'api/system/sms_setting','smsSetting',1,0),(44,9,'OssSetting','',1,'OSS存储','el_tuceng-8','',2,60,'api/system/oss_setting','ossSetting',1,0),(45,9,'PaymentSetting','',1,'支付设置','el_zhifu','',2,30,'','paymentSetting',1,0),(46,9,'EmailSetting','',1,'邮件设置','el_em','',2,70,'api/system/email_setting','emailSetting',1,0),(47,39,'menu_ext','',1,'更多','','',3,50,'','',0,0),(48,36,'ads_cate_edit','',1,'编辑分类','','',2,30,'api/ads/edit_cate','ItemEdit',0,1),(49,29,'ads_add','',1,'广告新增','','',2,20,'api/ads/add','ItemEdit',0,1),(50,36,'ads_cate_ext','',1,'更多操作','','',3,40,'','',0,0),(51,50,'ads_cate_del','',1,'删除','','',3,50,'api/ads/del_cate','',0,0),(52,47,'menu_copy','',1,'菜单复制','','',3,10,'api/menu/menu_edit','',1,1),(53,0,'persional','PageView',1,'个人页','el_kehudengji','',2,9001,'','',1,1),(54,53,'PersionalSetting','',1,'个人设置','el_wenzhangguanli','/persional/Setting/BaseSetting',2,1,'api/persional/setting','Setting',1,1),(55,54,'BaseSettings','',1,'基础设置','','',2,3,'api/persional/base_setting','BaseSetting',1,1),(56,54,'SecuritySettings','',1,'安全设置','','',2,5,'api/persional/security_setting','Security',1,1),(57,54,'AccountPassword','',1,'修改密码','','',2,8,'api/persional/change_pwd','Password',1,1),(58,54,'PhoneSettings','',1,'修改手机号','','',2,8,'api/persional/change_phone','Phone',1,1),(59,9,'SysSetting','',1,'系统设置','el_guanli','',2,1,'api/system/sys_setting','',1,0),(60,54,'BindingSettings','',1,'账户绑定','','',2,5,'api/persional/security_setting','Binding',1,1),(62,9,'PushSetting','',1,'消息推送设置','el_duanxinshezhi','',2,50,'api/system/push_setting','pushSetting',1,0);
/*!40000 ALTER TABLE `ql_auth_menu` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:24
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_manager`
--

DROP TABLE IF EXISTS `ql_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_manager` (
  `id` int NOT NULL AUTO_INCREMENT,
  `phone` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `salt` char(7) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT 'salt',
  `create_time` int DEFAULT '0' COMMENT '创建时间',
  `login_time` int DEFAULT '0' COMMENT '最新登录时间',
  `login_times` int DEFAULT '0' COMMENT '登录次数',
  `status` smallint DEFAULT '1' COMMENT '账号状态',
  `login_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '登录IP',
  `is_admin` smallint DEFAULT '0' COMMENT '主管理员',
  `groupId` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '权限组',
  `manager_id` int DEFAULT '0' COMMENT '所属主账号',
  `alpha_dev` smallint DEFAULT '1' COMMENT '1=无，2=开发者，3=内测用户',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像',
  `realName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `wechat_open_id` varchar(64) DEFAULT NULL,
  `wechat_union_id` varchar(64) DEFAULT NULL,
  `wechat_nickname` varchar(32) DEFAULT NULL,
  `ww_user_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `phone` (`phone`),
  KEY `manager_id` (`manager_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_manager`
--

/*!40000 ALTER TABLE `ql_manager` DISABLE KEYS */;
INSERT INTO `ql_manager` VALUES (1,'13235811184','efcba4a615a55d727eaf130039a8df75','jwV9%=;',1552560891,1688718437,189,1,'119.162.7.176',1,'1',1,1,'dragon',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'13235811111','0799aa35621be33beff1fd9b615b68d1','kOYZ!@=',1619322128,1619339710,10,1,'127.0.0.1',0,'2',1,1,'dragon8','','',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ql_manager` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:25
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_send_sms_log`
--

DROP TABLE IF EXISTS `ql_send_sms_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_send_sms_log` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '服务ID',
  `template_param` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务项名称',
  `status` smallint NOT NULL DEFAULT '1' COMMENT '状态。1有效，-1无效',
  `send_time` int NOT NULL COMMENT '添加时间',
  `sms_tpl_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `uid` bigint DEFAULT NULL,
  `phone` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `sign` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status_msg` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='短信发送日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_send_sms_log`
--

/*!40000 ALTER TABLE `ql_send_sms_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `ql_send_sms_log` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:26
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_alioss_setting`
--

DROP TABLE IF EXISTS `ql_sys_alioss_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_alioss_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dev_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `fs_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fc_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `secret_key` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `open` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_alioss_setting`
--

/*!40000 ALTER TABLE `ql_sys_alioss_setting` DISABLE KEYS */;
INSERT INTO `ql_sys_alioss_setting` VALUES (1,'xx','xxx','xxxx','xxx',1);
/*!40000 ALTER TABLE `ql_sys_alioss_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:27
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_alipay_ecmpay`
--

DROP TABLE IF EXISTS `ql_sys_alipay_ecmpay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_alipay_ecmpay` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_id` varchar(64) DEFAULT NULL,
  `auth_app_id` varchar(64) DEFAULT NULL COMMENT '授权应用ID',
  `app_secret` text COMMENT '应用密钥',
  `notify_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_alipay_ecmpay`
--

/*!40000 ALTER TABLE `ql_sys_alipay_ecmpay` DISABLE KEYS */;
INSERT INTO `ql_sys_alipay_ecmpay` VALUES (1,'47271','2021xx','9ddd4e0871xxxx','http://www.ecmpay.cn');
/*!40000 ALTER TABLE `ql_sys_alipay_ecmpay` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:27
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_alipay_setting`
--

DROP TABLE IF EXISTS `ql_sys_alipay_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_alipay_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_id` varchar(32) DEFAULT NULL,
  `application_private_key` text COMMENT '应用私钥',
  `alipay_public_key` text COMMENT '支付宝公钥',
  `alipay_cert_path` varchar(255) DEFAULT NULL,
  `alipay_root_cert_path` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1' COMMENT '模式，1=证书模式，2=普通模式',
  `notify_url` varchar(255) DEFAULT NULL,
  `application_cert_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_alipay_setting`
--

/*!40000 ALTER TABLE `ql_sys_alipay_setting` DISABLE KEYS */;
INSERT INTO `ql_sys_alipay_setting` VALUES (1,'xx','xx','xxx','xx','xx',1,'','xx');
/*!40000 ALTER TABLE `ql_sys_alipay_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:28
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_alisms_setting`
--

DROP TABLE IF EXISTS `ql_sys_alisms_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_alisms_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `key_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `key_secret` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '密钥',
  `region` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地区',
  `sign` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '签名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_alisms_setting`
--

/*!40000 ALTER TABLE `ql_sys_alisms_setting` DISABLE KEYS */;
INSERT INTO `ql_sys_alisms_setting` VALUES (1,'xxxx','xxxx','cn-hangzhou','xxx');
/*!40000 ALTER TABLE `ql_sys_alisms_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:29
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_payment_setting`
--

DROP TABLE IF EXISTS `ql_sys_payment_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_payment_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pay_type` varchar(64) DEFAULT NULL COMMENT '支付方式',
  `pay_platform` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='支付设置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_payment_setting`
--

/*!40000 ALTER TABLE `ql_sys_payment_setting` DISABLE KEYS */;
INSERT INTO `ql_sys_payment_setting` VALUES (1,'wxpay,alipay','ecmpay');
/*!40000 ALTER TABLE `ql_sys_payment_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:30
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_push_setting`
--

DROP TABLE IF EXISTS `ql_sys_push_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_push_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dev_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `ps_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '密钥',
  `secret_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地区',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_push_setting`
--

/*!40000 ALTER TABLE `ql_sys_push_setting` DISABLE KEYS */;
INSERT INTO `ql_sys_push_setting` VALUES (1,'1','1',NULL);
/*!40000 ALTER TABLE `ql_sys_push_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:30
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_setting`
--

DROP TABLE IF EXISTS `ql_sys_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `wechat_appid` varchar(32) DEFAULT NULL,
  `wechat_secret` varchar(64) DEFAULT NULL,
  `dragon_captcha_id` varchar(64) DEFAULT NULL,
  `dragon_secret_key` varchar(64) DEFAULT NULL,
  `ww_appid` varchar(32) DEFAULT NULL,
  `ww_agentid` varchar(32) DEFAULT NULL,
  `ww_agent_secret` varchar(64) DEFAULT NULL,
  `sms_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统设置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_setting`
--

/*!40000 ALTER TABLE `ql_sys_setting` DISABLE KEYS */;
INSERT INTO `ql_sys_setting` VALUES (1,'x','x','x','x','x','x','-DdkxxxYxxx0vk',NULL);
/*!40000 ALTER TABLE `ql_sys_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:31
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_sms_setting`
--

DROP TABLE IF EXISTS `ql_sys_sms_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_sms_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dev_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `ss_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '密钥',
  `secret_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地区',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_sms_setting`
--

/*!40000 ALTER TABLE `ql_sys_sms_setting` DISABLE KEYS */;
INSERT INTO `ql_sys_sms_setting` VALUES (1,'xxxx','xxxx','cn-hangzhou');
/*!40000 ALTER TABLE `ql_sys_sms_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:32
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_smtp_setting`
--

DROP TABLE IF EXISTS `ql_sys_smtp_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_smtp_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dev_key` varchar(128) DEFAULT NULL COMMENT '开发者key',
  `es_id` varchar(32) DEFAULT NULL COMMENT '邮件服务器ID',
  `et_id` varchar(32) DEFAULT NULL COMMENT '邮件模板ID',
  `secret_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='SMTP配置';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_smtp_setting`
--

/*!40000 ALTER TABLE `ql_sys_smtp_setting` DISABLE KEYS */;
INSERT INTO `ql_sys_smtp_setting` VALUES (1,'xxx','xxxx','','xxxx');
/*!40000 ALTER TABLE `ql_sys_smtp_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:33
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_wxpay_ecmpay`
--

DROP TABLE IF EXISTS `ql_sys_wxpay_ecmpay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_wxpay_ecmpay` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_id` varchar(32) DEFAULT NULL COMMENT '应用ID',
  `mch_id` varchar(32) DEFAULT NULL COMMENT '商户号',
  `notify_url` varchar(255) DEFAULT NULL,
  `mch_type` varchar(32) DEFAULT NULL,
  `app_secret` text,
  `mch_no` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci AVG_ROW_LENGTH=8192 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_wxpay_ecmpay`
--

/*!40000 ALTER TABLE `ql_sys_wxpay_ecmpay` DISABLE KEYS */;
INSERT INTO `ql_sys_wxpay_ecmpay` VALUES (1,'1','345353543','','mch','5345435353',NULL);
/*!40000 ALTER TABLE `ql_sys_wxpay_ecmpay` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:34
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_sys_wxpay_setting`
--

DROP TABLE IF EXISTS `ql_sys_wxpay_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_sys_wxpay_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_id` varchar(32) DEFAULT NULL COMMENT '应用ID',
  `mch_id` varchar(32) DEFAULT NULL COMMENT '商户号',
  `notify_url` varchar(255) DEFAULT NULL,
  `merchant_private_key` varchar(255) DEFAULT NULL COMMENT '商户私钥',
  `api_v3_code` varchar(255) DEFAULT NULL,
  `merchant_certificate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_sys_wxpay_setting`
--

/*!40000 ALTER TABLE `ql_sys_wxpay_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `ql_sys_wxpay_setting` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:34
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_users`
--

DROP TABLE IF EXISTS `ql_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `openid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户OPENID',
  `sex` smallint NOT NULL DEFAULT '0' COMMENT '性别',
  `province` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `headimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像',
  `subscribe_time` int DEFAULT '0' COMMENT '关注时间',
  `unionid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT 'UNIONID',
  `create_time` int DEFAULT NULL COMMENT '创建时间',
  `update_time` int DEFAULT NULL COMMENT '更新时间',
  `is_subscribe` smallint DEFAULT '0' COMMENT '是否关注：1 关注，0 未关注',
  `phone` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '手机号',
  `loginnum` int DEFAULT '0',
  `login_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `login_time` int DEFAULT '0',
  `status` smallint DEFAULT '1',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `salt` char(7) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `normal` (`openid`,`unionid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='客户数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_users`
--

/*!40000 ALTER TABLE `ql_users` DISABLE KEYS */;
INSERT INTO `ql_users` VALUES (1,'EC龙九	','1',1,NULL,NULL,NULL,0,'0',NULL,NULL,0,'0',0,'0',0,1,NULL,NULL);
/*!40000 ALTER TABLE `ql_users` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:35
-- ************************************************************
--
-- close fk
--
-- skip


-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 9.236.36.32    Database: qianlong_cms_os
-- ------------------------------------------------------
-- Server version	8.0.22-cynos

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ql_wechat_certificates`
--

DROP TABLE IF EXISTS `ql_wechat_certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ql_wechat_certificates` (
  `id` int NOT NULL AUTO_INCREMENT,
  `serial_no` varchar(64) DEFAULT NULL COMMENT '证书序列号',
  `effective_time` int DEFAULT NULL COMMENT '有效时间',
  `expire_time` int DEFAULT NULL COMMENT '过期时间',
  `certificate` text COMMENT '证书内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='微信平台证书列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ql_wechat_certificates`
--

/*!40000 ALTER TABLE `ql_wechat_certificates` DISABLE KEYS */;
/*!40000 ALTER TABLE `ql_wechat_certificates` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-07 18:01:36
