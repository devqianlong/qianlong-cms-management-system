<?php

use app\api\model\AuthGroup;
use think\facade\Db;
use app\common\service\QlRedis;

function checkSign()
{
  $appid = request()->header('x-access-appid');
  $time = request()->header('x-access-ts');
  if (empty($appid)) {
    $data['code'] = -1;
    $data['data'] = '';
    $data['msg'] = '授权KEY丢失';
    echo json_encode($data);
    exit;
  }
  if (empty($time)) {
    $data['code'] = -1;
    $data['data'] = '';
    $data['msg'] = '请求时间丢失';
    echo json_encode($data);
    exit;
  }
  // 实时数据
  $app = Db::name('appid')->where(['app_id' => $appid])->find();
  //认证：状态
  if ($app['is_enable'] != 1) {
    $data['code'] = -1;
    $data['data'] = '';
    $data['msg'] = '授权KEY已被禁用';
    echo json_encode($data);
    exit;
  }
  // 接口签名认证
  if (config("SYS_STATIC.app_sign_auth_on") === true) {
    $signature = input("signature"); // app端生成的签名
    $param     = input("param.");
    unset($param['signature']);
    if (empty($signature)) {
      $data['code'] = -1;
      $data['data'] = '';
      $data['msg'] = '签名不存在';
      echo json_encode($data);
      exit;
    }
    //数组排序
    ksort($param);
    $str        = http_build_query($param);
    $signature1 = md5(hash("sha256", $str) . $app['app_secret']);
    if ($signature != $signature1) {
      $data['code'] = -1;
      $data['data'] = '';
      $data['msg'] = '签名不匹配';
      echo json_encode($data);
      exit;
    }
  }
}
function dataTuomin($string, $start = 0, $length = 0, $re = '*')
{
  if (empty($string)) {
    return false;
  }
  $strarr = array();
  $mb_strlen = mb_strlen($string);
  while ($mb_strlen) { //循环把字符串变为数组
    $strarr[] = mb_substr($string, 0, 1, 'utf8');
    $string = mb_substr($string, 1, $mb_strlen, 'utf8');
    $mb_strlen = mb_strlen($string);
  }
  $strlen = count($strarr);
  $begin = $start >= 0 ? $start : ($strlen - abs($start));
  $end = $last = $strlen - 1;
  if ($length > 0) {
    $end = $begin + $length - 1;
  } elseif ($length < 0) {
    $end -= abs($length);
  }
  for ($i = $begin; $i <= $end; $i++) {
    $strarr[$i] = $re;
  }
  if ($begin >= $end || $begin >= $last || $end > $last) return false;
  return implode('', $strarr);
}
/**
 * 获取制定用户组信息
 * @param string $id  用户组ID
 * @param string $mid 商户ID
 * @Author Qianlong <87498106@qq.com>
 * @PersonSite http://dev.21ds.cn/
 */
function getAuthGroupById($id, $mid)
{
  $listData = getRedisCache('authGroup:id:' . $mid . '-' . $id, 'json');
  if (empty($listData)) {
    $listData = AuthGroup::field('*')->where('id', $id)->where('mid', $mid)->find();
    unset($listData['mid']);
    putRedisCache('authGroup:id:' . $mid . '-' . $id, $listData, config('app.SYS_STATIC.redis_expire'), 'json');
  }
  return $listData;
}
/**
 * 获取管理用户角色列表
 * @param [type] $mid
 * @Author Qianlong <87498106@qq.com>
 * @PersonSite http://dev.21ds.cn/
 */
function getAuthGroupListByMid($mid)
{
  $listData = getRedisCache('authGroupList:Mid:' . $mid, 'json');
  if (empty($listData)) {
    $listData = AuthGroup::field('*')->where('mid', $mid)->select()->toArray();
    unset($listData['mid']);
    putRedisCache('authGroupList:Mid:' . $mid, $listData, config('app.SYS_STATIC.redis_expire'), 'json');
  }
  return $listData;
}
      
function listToTreeMulti($list, $root = 0, $pk = 'id', $pid = 'parentId', $child = 'child', $path = 'path', $lastPath = '')
{
  $tree = array();
  foreach ($list as $v) {
    if ($v[$pid] == $root) {
      if (isset($v['filePath'])) {
        $v[$path] = $lastPath . '/' . $v['filePath'];
      } else {
        $v[$path] = '';
      }
      $v[$child] = listToTreeMulti($list, $v[$pk], $pk, $pid, $child, $path, $v[$path]);
      $tree[]    = $v;
    }
  }
  return $tree;
}
// 读取redis缓存
function getRedisCache($field, $type='string')
{
    $redis = new QlRedis();
    if ($redis->exists($field) > 0) {
        $data_s = $redis->get($field);
        if ($type == 'json') {
            $data_s = json_decode($data_s, true);
        }
        if (empty($data_s)) {
            return '';
        } else {
            return $data_s;
        }
    } else {
        return '';
    }
}
// 将数据存入redis缓存
function putRedisCache($field, $data='', $expire=-1, $type='string')
{
    if (empty($data)) {
        return true;
    }
    $redis = new QlRedis();
    if ($type == 'json') {
        $data = json_encode($data, true);
    }
    $redis->set($field, $data, $expire);
}
// 删除redis缓存
function delRedisCache($field)
{
    $redis = new QlRedis();
    $redis->del($field);
}
