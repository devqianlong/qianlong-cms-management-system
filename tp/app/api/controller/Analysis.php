<?php
/*
 * @Description    : 分析页
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-05-10 10:28:51
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-10 11:22:51
 */

namespace app\api\controller;

use app\lib\Result;

class Analysis extends Base
{
  /**
   * 图表数据
   * @Author Qianlong <87498106@qq.com>
   * @PersonSite http://dev.21ds.cn/
   */
  public function index()
  {
    $ageData['type'] = '未知';
    $ageData['value'] = 654;
    $ageData['percent'] = 0.02;
    $return['userAge'][] = $ageData;
    $ageData['type'] = '17 岁以下';
    $ageData['value'] = 654;
    $ageData['percent'] = 0.02;
    $return['userAge'][] = $ageData;
    $ageData['type'] = '18-24 岁';
    $ageData['value'] = 4400;
    $ageData['percent'] = 0.2;
    $return['userAge'][] = $ageData;
    $ageData['type'] = '25-29 岁';
    $ageData['value'] = 5300;
    $ageData['percent'] = 0.24;
    $return['userAge'][] = $ageData;
    $ageData['type'] = '30-39 岁';
    $ageData['value'] = 6200;
    $ageData['percent'] = 0.28;
    $return['userAge'][] = $ageData;
    $ageData['type'] = '40-49 岁';
    $ageData['value'] = 3300;
    $ageData['percent'] = 0.14;
    $return['userAge'][] = $ageData;
    $ageData['type'] = '50 岁以上';
    $ageData['value'] = 1500;
    $ageData['percent'] = 0.06;
    $return['userAge'][] = $ageData;
    $saleData['type'] = '一线城市';
    $saleData['value'] = 0.19;
    $return['salesRate'][] = $saleData;
    $saleData['type'] = '二线城市';
    $saleData['value'] = 0.21;
    $return['salesRate'][] = $saleData;
    $saleData['type'] = '三线城市';
    $saleData['value'] = 0.27;
    $return['salesRate'][] = $saleData;
    $saleData['type'] = '四线及以下';
    $saleData['value'] = 0.33;
    $return['salesRate'][] = $saleData;
    $monthData['month'] = '一月';
    $monthData['value'] = 1.6;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '二月';
    $monthData['value'] = 1.65;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '三月';
    $monthData['value'] = 1.8;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '四月';
    $monthData['value'] = 1.9;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '五月';
    $monthData['value'] = 2.01;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '六月';
    $monthData['value'] = 2.3;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '七月';
    $monthData['value'] = 2.35;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '八月';
    $monthData['value'] = 2.5;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '九月';
    $monthData['value'] = 2.6;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '十月';
    $monthData['value'] = 2.2;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '十一月';
    $monthData['value'] = 3.2;
    $return['salesMonth'][] = $monthData;
    $monthData['month'] = '十二月';
    $monthData['value'] = 2.9;
    $return['salesMonth'][] = $monthData;
    return Result::Success($return);
  }
}
