<?php
/*
 * @Description    : 
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-03-30 13:20:04
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-06 11:15:37
 */

namespace app\api\controller;

use app\api\model\AuthMenu;
use think\facade\Validate;
use app\common\logic\Data;

class Menu extends Base
{
  public function auth_menu()
  {
    $rule = [
      'title|菜单名' => 'max:10',
    ];
    $data = [
      'title'  => input('param.title', '', 'trim'),
    ];
    $validate = Validate::rule($rule);
    $result   = $validate->check($data);
    if (!$result) {
      return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
    }
    $where = [];
    if (!empty($data['title'])) {
      $where[] = ['title','like','%'.$data['title'].'%'];
    }
    $rulesList = AuthMenu::where($where)->order('sort', ' asc')->select()->toArray();
    foreach ($rulesList as &$vo) {
      $vo['id'] = (string) $vo['id'];
      $vo['pageTag'] = $vo['key'];
      $vo['key'] = $vo['id'];
      // if ($vo['type'] == 3) {
      //   $vo['isLeaf'] = true;
      // }
      switch ($vo['type']) {
        case '1':
          $type = '目录';
          break;
        case '2':
          $type = '菜单';
          break;
        case '3':
          $type = '操作';
          break;
        default:
          break;
      }
      switch ($vo['status']) {
        case '1':
          $status = '';
          break;
        case '-1':
          $status = '[禁用] ';
          break;
        case '2':
          $status = '[开发版] ';
          break;
        case '3':
          $status = '[内测版] ';
          break;
        default:
          $status = '';
          break;
      }
      $vo['type_msg'] = $type;
      $vo['status_msg'] = $status;
    }
    $sdata = new Data();
    $selectResult = $sdata->listToTreeMulti($rulesList, 0, 'id', 'pid', 'children');
    $selectResult = $sdata->filterEmptyKey($selectResult, 'children');
    if (empty($selectResult)) {
      $selectResult = $rulesList;
    }
    $return['code'] = 200;
    $return['data'] = $selectResult;
    return json($return);
  }
  public function getMenuWithLevel()
  {
    $selectResult = AuthMenu::order('sort', 'desc')->select()->toArray();
    $sdata = new Data();
    foreach ($selectResult as &$vo) {
      switch ($vo['type']) {
        case '1':
          $type = '目录';
          break;
        case '2':
          $type = '菜单';
          break;
        case '3':
          $type = '操作';
          break;
        default:
          $type = '';
          break;
      }
      switch ($vo['status']) {
        case '1':
          $status = '';
          break;
        case '-1':
          $status = '[禁用] ';
          break;
        case '2':
          $status = '[开发版] ';
          break;
        case '3':
          $status = '[内测版] ';
          break;
        default:
          $status = '';
          break;
      }
      $vo['disabled'] = ($vo['status'] == -1) ? true : false;
      $vo['title'] = $status . $vo['title'] . '【' . $type . '】';
      $vo['ids'] = join(',', $sdata->getArrSubIds($selectResult, $vo['id']));
    }
    $selectResult = $sdata->arr2table($selectResult);
    $return['data'] = $selectResult;
    $return['code'] = 200;
    return json($return);
  }
  public function menu_edit()
  {
    if (request()->isPost()) {
      $rule = [
        'pid|上级菜单' => 'require|number',
        'id|必要信息' => 'number',
        'pageTag|上级菜单' => 'require',
        'title|菜单名' => 'require',
        'filePath|页面路径' => 'alphaDash',
        'component|组件' => 'alphaDash',
        'sort|排序' => 'require|number',
        'hideChildrenInMenu|隐藏下级子菜单' => 'require|number',
        'hidden|隐藏本菜单' => 'require|number',
        'type|菜单类型' => 'require|number',
        'status|状态' => 'require|number',
        'operate_type|操作类型' => 'require|alphaDash',
      ];
      $data = [
        'pid'  => input('param.pid', '', 'trim'),
        'id'  => input('param.id', '', 'trim'),
        'title'  => input('param.title', '', 'trim'),
        'operate_type'  => input('param.operate_type', '', 'trim'),
        'pageTag'  => input('param.pageTag/s', '', 'trim'),
        'filePath'  => input('param.filePath/s', '', 'trim'),
        'component'  => input('param.component/s', '', 'trim'),
        'icon'  => input('param.icon', '', 'trim'),
        'redirect'  => input('param.redirect/s', '', 'trim'),
        'actionUrl'  => input('param.actionUrl/s', '', 'trim'),
        'sort'  => input('param.sort/d', '', 'trim'),
        'hideChildrenInMenu'  => input('param.hideChildrenInMenu/d', '', 'trim'),
        'hidden'  => input('param.hidden/d', '', 'trim'),
        'type'  => input('param.type/d', '', 'trim'),
        'status'  => input('param.status/d', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      if ($data['operate_type'] == 'edit' && $data['id'] < 0) {
        return json(['code' => -1, 'data' => '', 'msg' => '缺少必要条件']);
      }
      unset($data['operate_type']);
      $has_key = AuthMenu::where('key', $data['pageTag'])->find();
      if (!empty($has_key)) {
        if ($has_key['id'] != $data['id']) {
          return json(['code' => -1, 'data' => '', 'msg' => $data['pageTag'] . '已存在，菜单名：' . $has_key['title']]);
        }
      }
      if (!empty($data['actionUrl'])) {
        $data['actionUrl'] = parse_name($data['actionUrl']);
      }
      $data['key'] = $data['pageTag'];
      unset($data['pageTag']);
      if (empty($data['id'])) {
        unset($data['id']);
        $status = AuthMenu::insert($data);
        $optype = '新增';
      } else {
        $status = AuthMenu::where('id', $data['id'])->update($data);
        $optype = '编辑';
      }
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => $optype . '成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => $optype . '失败，请重试']);
      }
    }
    return json(['code' => -1, 'data' => '', 'msg' => '请求方式不对']);
  }
  public function menu_del()
  {
    $id = input('param.id');
    if (empty($id)) {
      return json(['code' => -1, 'data' => '', 'msg' => '必要信息不能为空']);
    }
    $hasSub = AuthMenu::where('pid', $id)->find();
    if (!empty($hasSub)) {
      return json(['code' => -1, 'data' => '', 'msg' => '该菜单下包含其他菜单项，不允许删除']);
    }
    $flag = AuthMenu::where('id', $id)->delete();
    if ($flag > 0) {
      return json(['code' => 200, 'data' => '', 'msg' => '删除成功']);
    } else {
      return json(['code' => -1, 'data' => '', 'msg' => '删除失败，请重试']);
    }
  }
}
