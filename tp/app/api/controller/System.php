<?php
/*
 * @Description: 系统管理
 * @Author: QianLong
 * @Date: 2019-08-07 22:14:56
 * @LastEditors    : QianLong
 * @LastEditTime   : 2023-07-07 17:44:02
 */

namespace app\api\controller;

use app\api\model\SysSetting;
use app\common\model\SysPaymentSetting;
use app\common\model\SysPushSetting;
use app\common\model\SysSmsSetting;
use app\common\service\Email;
use app\lib\Result;
use think\facade\Validate;
use think\facade\Db;

class System extends Base
{
  /**
   * 短信设置
   * @Author Qianlong <87498106@qq.com>
   * @PersonSite http://www.21ds.cn/
   */
  public function sms_setting()
  {
    if (request()->isPost()) {
      $rule = [
        'dev_key|开发者key'  => 'require',
        'ss_id|短信配置ID'  => 'require',
        'secret_key|SecretKey'  => 'require',
      ];
      $data = [
        'dev_key' => input('param.dev_key', '', 'trim'),
        'ss_id' => input('param.ss_id', '', 'trim'),
        'secret_key' => input('param.secret_key', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $this->redis->del('system:sms:setting');
      $status = SysSmsSetting::where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    }else{
      $data = SysSmsSetting::where('id', 1)->find();
      return json(['code' => 200, 'data' => $data, 'msg' => 'success']);
    }
  }
  /**
   * 消息推送设置
   * @Author Qianlong <87498106@qq.com>
   * @PersonSite http://www.21ds.cn/
   */
  public function push_setting()
  {
    if (request()->isPost()) {
      $rule = [
        'dev_key|开发者key'  => 'require',
        'ps_id|消息推送ID'  => 'require',
        'secret_key|SecretKey'  => 'require',
      ];
      $data = [
        'dev_key' => input('param.dev_key', '', 'trim'),
        'ps_id' => input('param.ps_id', '', 'trim'),
        'secret_key' => input('param.secret_key', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $this->redis->del('system:push:setting');
      $status = SysPushSetting::where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    }else{
      $data = SysPushSetting::where('id', 1)->find();
      return json(['code' => 200, 'data' => $data, 'msg' => 'success']);
    }
  }
  /**
   * 系统设置
   * @Author Qianlong <87498106@qq.com>
   * @PersonSite http://www.21ds.cn/
   */
  public function sys_setting()
  {
    if (request()->isPost()) {
      $rule = [
        'wechat_appid|微信登录APPID'  => 'alphaDash',
        'wechat_secret|微信登录Secret'  => 'alphaDash',
        'ww_appid|企业微信CorpID'  => 'alphaDash',
        'ww_agentid|企业微信网页应用ID'  => 'number',
      ];
      $data = [
        'ww_appid' => input('param.ww_appid', '', 'trim'),
        'ww_agentid' => input('param.ww_agentid', '', 'trim'),
        'ww_agent_secret' => input('param.ww_agent_secret', '', 'trim'),
        'wechat_appid' => input('param.wechat_appid', '', 'trim'),
        'wechat_secret' => input('param.wechat_secret', '', 'trim'),
        'dragon_captcha_id' => input('param.dragon_captcha_id', '', 'trim'),
        'dragon_secret_key' => input('param.dragon_secret_key', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $status = Db::name('SysSetting')->where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    } else {
      $infoData = Db::name('SysSetting')->where('id', 1)->find();
      return json(['code' => 200, 'data' => $infoData, 'msg' => '获取成功']);
    }
  }
  /**
   * 邮件设置
   * @return void
   * @author QianLong <87498106@qq.com>
   * @date 2021-04-17 10:34:05
   * @editAuthor QianLong <87498106@qq.com>
   * @editDescription 
   * @editDate 2021-04-17 10:34:05
   */
  public function email_setting()
  {
    if (request()->isPost()) {
      $rule = [
        'dev_key|开发者key'  => 'require',
        'es_id|邮件服务器ID'  => 'require',
        'secret_key|SecretKey'  => 'require',
      ];
      $data = [
        'dev_key' => input('param.dev_key', '', 'trim'),
        'es_id' => input('param.es_id', '', 'trim'),
        'et_id' => input('param.et_id', '', 'trim'),
        'secret_key' => input('param.secret_key', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $this->redis->del('system:smtp:setting');
      $status = Db::name('SysSmtpSetting')->where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    } else {
      $infoData = Db::name('SysSmtpSetting')->where('id', 1)->find();
      return json(['code' => 200, 'data' => $infoData, 'msg' => '获取成功']);
    }
  }
  /**
   * 邮件测试
   * @return void
   * @author QianLong <87498106@qq.com>
   * @date 2021-04-17 10:51:03
   * @editAuthor QianLong <87498106@qq.com>
   * @editDescription 
   * @editDate 2021-04-17 10:51:03
   */
  public function email_test()
  {
    if (request()->isPost()) {
      $rule = [
        'email|收件人邮箱地址'  => 'require|email',
      ];
      $data = [
        'email' => input('param.test_email', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $info = Db::name('SysSmtpSetting')->where('id', 1)->find();
      if (empty($info)) {
        return Result::Error('请先进行配置');
      }
      $emailPost['title'] = '测试邮件';
      $emailPost['content'] = '这是测试邮件内容 ' . date('Y-m-d H:i:s');
      $emailPost['email'] = $data['email'];
      $result = doSendEmail($emailPost);
      if ($result['code'] == 200) {
        return Result::Success('发送成功');
      } else {
        return Result::Error('发送失败 ' . $result['msg']);
      }
    }
    return json(['code' => -1, 'data' => '', 'msg' => '请求方式不对']);
  }
  /**
   * OSS设置
   * @Author Qianlong <87498106@qq.com>
   * @PersonSite http://www.21ds.cn/
   */
  public function oss_setting()
  {
    if (request()->isPost()) {
      $rule = [
        'dev_key|开发者key'  => 'require',
        'fs_id|文件存储配置ID'  => 'require',
        'fc_id|文件存储分类ID'  => 'require',
        'secret_key|SecretKey'  => 'require',
      ];
      $data = [
        'dev_key' => input('param.dev_key', '', 'trim'),
        'open' => input('param.open', '', 'trim'),
        'fs_id' => input('param.fs_id', '', 'trim'),
        'fc_id' => input('param.fc_id', '', 'trim'),
        'secret_key' => input('param.secret_key', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $this->redis->del('system:oss:setting');
      $status = Db::name('SysAliossSetting')->where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    } else {
      $infoData = Db::name('SysAliossSetting')->where('id', 1)->find();
      return json(['code' => 200, 'data' => $infoData, 'msg' => '获取成功']);
    }
  }
  /**
   * 支付设置
   * @Author Qianlong <87498106@qq.com>
   * @PersonSite http://www.21ds.cn/
   */
  public function pay_setting()
  {
    $tab = input('param.tab/s', '', 'trim');
    if ($tab == 'tab1') {
      return $this->payment_setting();
    } else if ($tab == 'tab2') {
      $setting = SysPaymentSetting::where('id', 1)->find();
      if ($setting['pay_platform'] == 'ecmpay') {
        return $this->alipay_ecmpay();
      } else {
        return $this->alipay_setting();
      }
    } else if ($tab == 'tab3') {
      $setting = SysPaymentSetting::where('id', 1)->find();
      if ($setting['pay_platform'] == 'ecmpay') {
        return $this->wxpay_ecmpay();
      } else {
        return $this->wxpay_setting();
      }
    }
    return json(['code' => -1, 'data' => '', 'msg' => 'Tab必须选中一项']);
  }
  private function payment_setting()
  {
    if (request()->isPost()) {
      $rule = [
        'pay_type|支付方式'  => 'require',
      ];
      $data = [
        'pay_type' => input('param.pay_type/a', [], 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $data['pay_type'] = implode(',', $data['pay_type']);
      $this->redis->del('system:payment:setting');
      $status = SysPaymentSetting::where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    } else {
      $infoData = SysPaymentSetting::where('id', 1)->find();
      $infoData['pay_type'] = explode(',', $infoData['pay_type']);
      return json(['code' => 200, 'data' => $infoData, 'msg' => '获取成功']);
    }
  }
  private function alipay_ecmpay()
  {
    if (request()->isPost()) {
      $rule = [
        'app_id|APPId'  => 'require|number',
        'auth_app_id|授权的应用ID'  => 'require',
        'app_secret|应用私钥'  => 'require|max:255',
        'notify_url|异步通知地址'  => 'url',
      ];
      $data = [
        'app_id' => input('param.app_id', '', 'trim'),
        'auth_app_id' => input('param.auth_app_id', '', 'trim'),
        'app_secret' => input('param.app_secret', '', 'trim'),
        'notify_url' => input('param.notify_url', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $status = Db::name('SysAlipayEcmpay')->where('id', 1)->update($data);
      $this->redis->del('system:pay:setting:alipay');
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    } else {
      $infoData = Db::name('SysAlipayEcmpay')->where('id', 1)->find();
      return json(['code' => 200, 'data' => $infoData, 'msg' => '获取成功']);
    }
  }
  private function wxpay_ecmpay()
  {
    if (request()->isPost()) {
      $rule = [
        'app_id|应用Id'  => 'require',
        'mch_id|商户号'  => 'require|number',
        'mch_type|商户类别'  => 'require|max:255',
        'app_secret|应用私钥'  => 'require|max:255',
        'notify_url|异步通知地址'  => 'url',
      ];
      $data = [
        'app_id' => input('param.app_id', '', 'trim'),
        'mch_id' => input('param.mch_id', '', 'trim'),
        'mch_type' => input('param.mch_type', '', 'trim'),
        'app_secret' => input('param.app_secret', '', 'trim'),
        'notify_url' => input('param.notify_url', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      $this->redis->del('system:pay:setting:wxpay');
      $status = Db::name('SysWxpayEcmpay')->where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    } else {
      $infoData = Db::name('SysWxpayEcmpay')->where('id', 1)->find();
      return json(['code' => 200, 'data' => $infoData, 'msg' => '获取成功']);
    }
  }
  private function alipay_setting()
  {
    if (request()->isPost()) {
      $rule = [
        'app_id|APPId'  => 'require|number',
        'application_private_key|应用私钥'  => 'require',
        'type|接口加签方式'  => 'require|number',
        'alipay_root_cert_path|支付宝根证书'  => 'max:255',
        'application_cert_path|应用公钥证书'  => 'max:255',
        'alipay_cert_path|支付宝公钥证书'  => 'max:255',
        'notify_url|异步通知地址'  => 'url',
      ];
      $data = [
        'app_id' => input('param.app_id', '', 'trim'),
        'application_private_key' => input('param.application_private_key', '', 'trim'),
        'type' => input('param.type', '', 'trim'),
        'alipay_public_key' => input('param.alipay_public_key', '', 'trim'),
        'alipay_root_cert_path' => input('param.alipay_root_cert_path', '', 'trim'),
        'application_cert_path' => input('param.application_cert_path', '', 'trim'),
        'alipay_cert_path' => input('param.alipay_cert_path', '', 'trim'),
        'notify_url' => input('param.notify_url', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      if ($data['type'] == 1 && (empty($data['alipay_root_cert_path']) || empty($data['alipay_cert_path']))) {
        return json(['code' => -1, 'data' => '', 'msg' => '支付宝公钥证书及支付宝根证书必须都上传']);
      }
      if ($data['type'] == 2 && empty($data['alipay_public_key'])) {
        return json(['code' => -1, 'data' => '', 'msg' => '支付宝公钥必须填写']);
      }
      $this->redis->del('system:pay:setting:alipay');
      $status = Db::name('SysAlipaySetting')->where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    } else {
      $infoData = Db::name('SysAlipaySetting')->where('id', 1)->find();
      return json(['code' => 200, 'data' => $infoData, 'msg' => '获取成功']);
    }
  }
  private function wxpay_setting()
  {
    if (request()->isPost()) {
      $rule = [
        'app_id|应用Id'  => 'require',
        'mch_id|商户号'  => 'require|number',
        'merchant_private_key|商户私钥证书'  => 'require|max:255',
        'merchant_certificate|商户证书'  => 'require|max:255',
        'api_v3_code|api_v3_code'  => 'require|max:255',
        'notify_url|异步通知地址'  => 'url',
      ];
      $data = [
        'app_id' => input('param.app_id', '', 'trim'),
        'mch_id' => input('param.mch_id', '', 'trim'),
        'merchant_private_key' => input('param.merchant_private_key', '', 'trim'),
        'merchant_certificate' => input('param.merchant_certificate', '', 'trim'),
        'api_v3_code' => input('param.api_v3_code', '', 'trim'),
        'notify_url' => input('param.notify_url', '', 'trim'),
      ];
      $validate = Validate::rule($rule);
      $result   = $validate->check($data);
      if (!$result) {
        return json(['code' => -1, 'data' => '', 'msg' => $validate->getError()]);
      }
      if (empty($data['merchant_private_key'])) {
        return json(['code' => -1, 'data' => '', 'msg' => '商户私钥必须上传']);
      }
      $this->redis->del('system:pay:setting:wxpay');
      $status = Db::name('SysWxpaySetting')->where('id', 1)->update($data);
      if ($status > 0) {
        return json(['code' => 200, 'data' => '', 'msg' => '设置成功']);
      } else {
        return json(['code' => -1, 'data' => '', 'msg' => '设置失败，请重试']);
      }
    } else {
      $infoData = Db::name('SysWxpaySetting')->where('id', 1)->find();
      return json(['code' => 200, 'data' => $infoData, 'msg' => '获取成功']);
    }
  }
}
