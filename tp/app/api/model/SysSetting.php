<?php
/*
 * @Description    : 系统设置
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-05-05 09:01:30
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-10 17:40:53
 */
namespace app\api\model;

use think\Model;

class SysSetting extends Model
{
    // 模型初始化
    protected static function init()
    {
        //TODO:初始化内容
    }
}
