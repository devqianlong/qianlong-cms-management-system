<?php
/*
 * @Description    : redis缓存
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-16 16:48:24
 * @LastEditors    : QianLong
 * @LastEditTime   : 2023-07-07 17:44:20
 */

namespace app\common;

use app\common\model\SysAlipayEcmpay;
use app\common\model\SysAlipaySetting;
use app\common\model\SysPaymentSetting;
use app\common\model\SysPushSetting;
use app\common\model\SysSmsSetting;
use app\common\model\SysSmtpSetting;
use app\common\model\SysWxpayEcmpay;
use app\common\model\SysWxpaySetting;
use app\common\service\QlRedis;
use app\lib\exception\ApiException;
use think\facade\Db;

class RedisCache
{
    protected static $redis;
    public function __construct()
    {
        self::$redis = new QlRedis;
    }
    /**
     * 读取系统支付项配置
     */
    public static function getSysPaymentSetting()
    {
        $cacheTTL = self::$redis->ttl('system:payment:setting');
        if ($cacheTTL === -1 || $cacheTTL > config('app.SYS_STATIC.redis_expire')) {
            self::$redis->del('system:payment:setting');
        }
        if (self::$redis->exists('system:payment:setting') > 0) {
            $items = self::$redis->get('system:payment:setting');
            $items = json_decode($items, true);
        } else {
            $items = SysPaymentSetting::where('id', 1)->find();
            self::$redis->set('system:payment:setting', json_encode($items), config('app.SYS_STATIC.redis_expire'));
        }
        if (empty($items)) {
            throw new ApiException('系统配置为空');
        }
        return $items;
    }
    public static function getSysSmsSetting()
    {
        $cacheTTL = self::$redis->ttl('system:sms:setting');
        if ($cacheTTL === -1 || $cacheTTL > config('app.SYS_STATIC.redis_expire')) {
            self::$redis->del('system:sms:setting');
        }
        if (self::$redis->exists('system:sms:setting') > 0) {
            $items = self::$redis->get('system:sms:setting');
            $items = json_decode($items, true);
        } else {
            $items = SysSmsSetting::where('id', 1)->find();
            self::$redis->set('system:sms:setting', json_encode($items), config('app.SYS_STATIC.redis_expire'));
        }
        if (empty($items)) {
            throw new ApiException('短信配置为空');
        }
        return $items;
    }
    public static function getSysEmailSetting()
    {
        $cacheTTL = self::$redis->ttl('system:smtp:setting');
        if ($cacheTTL === -1 || $cacheTTL > config('app.SYS_STATIC.redis_expire')) {
            self::$redis->del('system:smtp:setting');
        }
        if (self::$redis->exists('system:smtp:setting') > 0) {
            $items = self::$redis->get('system:smtp:setting');
            $items = json_decode($items, true);
        } else {
            $items = SysSmtpSetting::where('id', 1)->find();
            self::$redis->set('system:smtp:setting', json_encode($items), config('app.SYS_STATIC.redis_expire'));
        }
        if (empty($items)) {
            throw new ApiException('邮件配置为空');
        }
        return $items;
    }
    public static function getSysPushSetting()
    {
        $cacheTTL = self::$redis->ttl('system:push:setting');
        if ($cacheTTL === -1 || $cacheTTL > config('app.SYS_STATIC.redis_expire')) {
            self::$redis->del('system:push:setting');
        }
        if (self::$redis->exists('system:push:setting') > 0) {
            $items = self::$redis->get('system:push:setting');
            $items = json_decode($items, true);
        } else {
            $items = SysPushSetting::where('id', 1)->find();
            self::$redis->set('system:push:setting', json_encode($items), config('app.SYS_STATIC.redis_expire'));
        }
        if (empty($items)) {
            throw new ApiException('消息推送配置为空');
        }
        return $items;
    }
    public static function getSysOssSetting()
    {
        $cacheTTL = self::$redis->ttl('system:oss:setting');
        if ($cacheTTL === -1 || $cacheTTL > config('app.SYS_STATIC.redis_expire')) {
            self::$redis->del('system:oss:setting');
        }
        if (self::$redis->exists('system:oss:setting') > 0) {
            $items = self::$redis->get('system:oss:setting');
            $items = json_decode($items, true);
        } else {
            $items = Db::name('SysAliossSetting')->where('id', 1)->find();
            self::$redis->set('system:oss:setting', json_encode($items), config('app.SYS_STATIC.redis_expire'));
        }
        if (empty($items)) {
            $items['open'] = 0;
        }
        return $items;
    }
    public static function getSysPaySetting($type)
    {
        $cacheTTL = self::$redis->ttl('system:pay:setting:' . $type);
        if ($cacheTTL === -1 || $cacheTTL > config('app.SYS_STATIC.redis_expire')) {
            self::$redis->del('system:pay:setting:' . $type);
        }
        $items = [];
        if (self::$redis->exists('system:pay:setting:' . $type) > 0) {
            $items = self::$redis->get('system:pay:setting:' . $type);
            $items = json_decode($items, true);
        } else {
            $resultPayment = self::getSysPaymentSetting();
            if (isset($resultPayment['pay_platform']) && $resultPayment['pay_platform'] == 'ecmpay') {
                if ($type == 'alipay') {
                    $items = SysAlipayEcmpay::where('id', 1)->find()->toArray();
                } elseif ($type == 'wxpay') {
                    $items = SysWxpayEcmpay::where('id', 1)->find()->toArray();
                }
            } else {
                if ($type == 'alipay') {
                    $items = SysAlipaySetting::where('id', 1)->find()->toArray();
                } elseif ($type == 'wxpay') {
                    $items = SysWxpaySetting::where('id', 1)->find()->toArray();
                }
            }
            self::$redis->set('system:pay:setting:' . $type, json_encode($items), config('app.SYS_STATIC.redis_expire'));
        }
        if (empty($items)) {
            throw new ApiException('支付配置为空');
        }
        return $items;
    }
}
