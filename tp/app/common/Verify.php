<?php
/*
 * @Description    : 验证
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-16 16:48:24
 * @LastEditors    : QianLong
 * @LastEditTime   : 2023-07-11 13:45:05
 */

namespace app\common;

use app\api\model\SysSetting;
use app\lib\exception\ApiException;

class Verify
{
  public static function check(array $postData)
  {
    $checkSetting = SysSetting::where('id', 1)->find();
    $apiUrl = 'http://21ds.cn/captcha/verify';
    if (!empty($checkSetting['dragon_captcha_id']) && strlen($checkSetting['dragon_captcha_id']) > 5 && !empty($checkSetting['dragon_secret_key'])) {
      $postData['captcha_id'] = $checkSetting['dragon_captcha_id'];
      $postData['sign'] = devSignGeneral($postData, $checkSetting['dragon_secret_key']);
      $http = new \Yurun\Util\HttpRequest;
      $response = $http->post($apiUrl, $postData);
      $result = json_decode($response->body(), true);
      if ($result['code'] != 200) {
        throw new ApiException('安全验证失败，请刷新页面后重试');
      }
    }
  }
}