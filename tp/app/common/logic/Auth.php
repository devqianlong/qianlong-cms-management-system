<?php
/*
 * @Description    : 权限
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-25 15:46:17
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-04-25 16:24:06
 */

namespace app\common\logic;

use think\facade\Db;

class Auth
{
    public static function getRuleId($groupId)
    {
        $ruleString = '';
        $ruleArr = Db::name('AuthGroup')->field('rules,halfrules')->whereIn('id', $groupId)->select()->toArray();
        foreach ($ruleArr as $key => $vo) {
            $ruleString = $ruleString . ',' . $vo['rules'] . ',' . $vo['halfrules'];
        }
        $ruleArr = explode(',', $ruleString);
        $ruleArr = array_unique($ruleArr);
        $ruleArr = array_filter($ruleArr);
        $ruleArr = implode(',', $ruleArr);
        return trim($ruleArr, ',');
    }
    public static function getRuleKey($ruleIds,$type='json')
    {
        $ruleString = [];
        $ruleArr = Db::name('AuthMenu')->field('key')->whereIn('id', $ruleIds)->select()->toArray();
        foreach ($ruleArr as $key => $vo) {
            $ruleString[] = $vo['key'];
        }
        $ruleArr = array_unique($ruleString);
        $ruleArr = array_filter($ruleArr);
        $ruleArr = implode(',', $ruleArr);
        if ($type == 'json') {
            return explode(',',trim($ruleArr, ','));
        }
        return trim($ruleArr, ',');
    }
}
