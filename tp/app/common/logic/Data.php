<?php

namespace app\common\logic;

class Data
{
    // public function getfirstchar($s0)
    // {
    //     $fchar = ord($s0{0});
    //     if ($fchar >= ord("A") and $fchar <= ord("z")) return strtoupper($s0{
    //         0});
    //     //$s1 = iconv("UTF-8","gb2312//IGNORE", $s0);
    //     // $s2 = iconv("gb2312","UTF-8//IGNORE", $s1);
    //     $s1 = $this->get_encoding($s0, 'GB2312');
    //     $s2 = $this->get_encoding($s1, 'UTF-8');
    //     if ($s2 == $s0) {
    //         $s = $s1;
    //     } else {
    //         $s = $s0;
    //     }
    //     $asc = ord($s{
    //         0}) * 256 + ord($s{
    //         1}) - 65536;
    //     // -24156
    //     if ($asc >= -20319 and $asc <= -20284) return "A";
    //     if ($asc >= -20283 and $asc <= -19776) return "B";
    //     if ($asc >= -19775 and $asc <= -19219) return "C";
    //     if ($asc >= -19218 and $asc <= -18711) return "D";
    //     if ($asc >= -18710 and $asc <= -18527) return "E";
    //     if ($asc >= -18526 and $asc <= -18240) return "F";
    //     if ($asc >= -18239 and $asc <= -17923) return "G";
    //     if ($asc >= -17922 and $asc <= -17418) return "I";
    //     if ($asc >= -17417 and $asc <= -16475) return "J";
    //     if ($asc >= -16474 and $asc <= -16213) return "K";
    //     if ($asc >= -16212 and $asc <= -15641) return "L";
    //     if ($asc >= -15640 and $asc <= -15166) return "M";
    //     if ($asc >= -15165 and $asc <= -14923) return "N";
    //     if ($asc >= -14922 and $asc <= -14915) return "O";
    //     if ($asc >= -14914 and $asc <= -14631) return "P";
    //     if ($asc >= -14630 and $asc <= -14150) return "Q";
    //     if ($asc >= -14149 and $asc <= -14091) return "R";
    //     if ($asc >= -14090 and $asc <= -13319) return "S";
    //     if ($asc >= -13318 and $asc <= -12839) return "T";
    //     if ($asc >= -12838 and $asc <= -12557) return "W";
    //     if ($asc >= -12556 and $asc <= -11848) return "X";
    //     if ($asc >= -11847 and $asc <= -11056) return "Y";
    //     if ($asc >= -11055 and $asc <= -10247) return "Z";
    //     return $asc;
    //     return null;
    // }
    /**
     * @name: get_encoding
     * @description: 自动检测内容编码进行转换
     * @param: string data
     * @param: string to  目标编码
     * @return: string
     **/
    public function get_encoding($data, $to)
    {
        $encode_arr = array('UTF-8', 'ASCII', 'GBK', 'GB2312', 'BIG5', 'JIS', 'eucjp-win', 'sjis-win', 'EUC-JP');
        $encoded = mb_detect_encoding($data, $encode_arr);
        $data = mb_convert_encoding($data, $to, $encoded);
        return $data;
    }

    // public function getPinyinSzm($zh)
    // {
    //     $ret = "";
    //     $s1 = iconv("UTF-8", "gb2312", $zh);
    //     $s2 = iconv("gb2312", "UTF-8", $s1);
    //     if ($s2 == $zh) {
    //         $zh = $s1;
    //     }
    //     for ($i = 0; $i < strlen($zh); $i++) {
    //         $s1 = substr($zh, $i, 1);
    //         $p = ord($s1);
    //         if ($p > 160) {
    //             $s2 = substr($zh, $i++, 2);
    //             $ret .= $this->getfirstchar($s2);
    //         } else {
    //             $ret .= $s1;
    //         }
    //     }
    //     return $ret;
    // }
    public static function listToTreeMulti($list, $root = 0, $pk = 'id', $pid = 'parentId', $child = 'child', $path = 'path', $lastpath = '')
    {
        $tree = array();
        foreach ($list as $v) {
            if ($v[$pid] == $root) {
                if (isset($v['filePath'])) {
                    $v[$path] = $lastpath . '/' . $v['filePath'];
                } else {
                    $v[$path] = '';
                }
                $v[$child] = self::listToTreeMulti($list, $v[$pk], $pk, $pid, $child, $path, $v[$path]);
                $tree[]    = $v;
            }
        }
        return $tree;
    }
    public static function filterEmptyKey($array, $key)
    {
        if (count($array) < 1) {
            return [];
        }
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $array[$k] = self::filterEmptyKey($v, $key);
            }
            if (empty($array[$k]) && $k = $key) {
                unset($array[$k]);
            }
        }
        return $array;
    }
    /**
     * 一维数据数组生成数据树
     * @param array $list 数据列表
     * @param string $id 父ID Key
     * @param string $pid ID Key
     * @param string $son 定义子数据Key
     * @return array
     */
    public static function arr2tree($list, $id = 'id', $pid = 'pid', $son = 'sub')
    {
        list($tree, $map) = [[], []];
        foreach ($list as $item) $map[$item[$id]] = $item;
        foreach ($list as $item) if (isset($item[$pid]) && isset($map[$item[$pid]])) {
            $map[$item[$pid]][$son][] = &$map[$item[$id]];
        } else $tree[] = &$map[$item[$id]];
        unset($map);
        return $tree;
    }

    /**
     * 一维数据数组生成数据树
     * @param array $list 数据列表
     * @param string $id ID Key
     * @param string $pid 父ID Key
     * @param string $path
     * @param string $ppath
     * @return array
     */
    public static function arr2table(array $list, $id = 'id', $pid = 'pid', $path = 'path', $ppath = '')
    {
        $tree = [];
        foreach (self::arr2tree($list, $id, $pid) as $attr) {
            $attr[$path] = "{$ppath}-{$attr[$id]}";
            $attr['sub'] = isset($attr['sub']) ? $attr['sub'] : [];
            $attr['spt'] = substr_count($ppath, '-');
            $attr['spl'] = str_repeat("　├　", $attr['spt']);
            $sub = $attr['sub'];
            unset($attr['sub']);
            $tree[] = $attr;
            if (!empty($sub)) $tree = array_merge($tree, self::arr2table($sub, $id, $pid, $path, $attr[$path]));
        }
        return $tree;
    }
    /**
     * 获取数据树子ID
     * @param array $list 数据列表
     * @param int $id 起始ID
     * @param string $key 子Key
     * @param string $pkey 父Key
     * @return array
     */
    public static function getArrSubIds($list, $id = 0, $key = 'id', $pkey = 'pid')
    {
        $ids = [intval($id)];
        foreach ($list as $vo) if (intval($vo[$pkey]) > 0 && intval($vo[$pkey]) === intval($id)) {
            $ids = array_merge($ids, self::getArrSubIds($list, intval($vo[$key]), $key, $pkey));
        }
        return $ids;
    }
    public static function camelToUnder($str)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . '_' . "$2", $str));
    }
}
