<?php
# @Author: 龙九
# @Date:   2017-03-31 10:30:29
# @Email:  838808603@qq.com
# @Last modified by:   QianLong
# @Last modified time: 2017-11-28 09:55:27
# @License: This is not a free software! You can modify and use the program code on the premise of not being used for commercial purposes.
# @Copyright: Copyright (c) 2015-3017 https://dev.21ds.cn All rights reserved.

namespace app\common\service;

class QlRedis
{
  protected $redis;
  protected $redis_db_prefix = '';
  public function __construct()
  {
    $this->redis_db_prefix = env('redis.prefix', 'ql_');
    $this->redis = new \Redis();
    $this->redis->connect(env('redis.host', '127.0.0.1'), env('redis.port', 6379));
    if (!empty(env('redis.password'))) {
      $this->redis->auth(env('redis.password'));
    }
  }

  public function hexists($key, $field)
  {
    $value = $this->redis->hexists($this->redis_db_prefix . $key, $field);
    return $value;
  }
  public function exists($key)
  {
    $value = $this->redis->exists($this->redis_db_prefix . $key);
    return $value;
  }

  public function hmset($key, $field, $value, $expire_date = 259200)
  {
    $this->redis->hset($this->redis_db_prefix . $key, $field, $value);
    $date = time() + $expire_date;
    $this->redis->expireat($this->redis_db_prefix . $key, $date);
  }
  public function set($key, $value, $expire_date = -1)
  {
    $this->redis->set($this->redis_db_prefix . $key, $value);
    if ($expire_date > 0) {
      $this->redis->expire($this->redis_db_prefix . $key, $expire_date);
    }
  }
  public function ttl($key)
  {
    $ttl = $this->redis->ttl($this->redis_db_prefix . $key);
    return $ttl;
  }

  public function del($key, $prefix = '')
  {
    if (empty($prefix)) {
      $this->redis->del($this->redis_db_prefix . $key);
    } else {
      if ($prefix == 'full') {
        $this->redis->del($key);
      } else {
        $this->redis->del($prefix . $key);
      }
    }
  }
  public function keys($key, $prefix = '')
  {
    if (empty($prefix)) {
      $keys = $this->redis->keys($this->redis_db_prefix . $key);
    } else {
      $keys = $this->redis->keys($prefix . $key);
    }
    return $keys;
  }
  public function incr($key, $incr = 1)
  {
    $this->redis->incr($this->redis_db_prefix . $key, $incr);
  }

  public function get($key)
  {
    $value = $this->redis->get($this->redis_db_prefix . $key);
    return $value;
  }

  public function hget($key, $field)
  {
    $value = $this->redis->hget($this->redis_db_prefix . $key, $field);
    return $value;
  }

  public function lpush($key, $field)
  {
    $value = $this->redis->lpush($this->redis_db_prefix . $key, $field);
    return $value;
  }

  public function llen($key)
  {
    $value = $this->redis->llen($this->redis_db_prefix . $key);
    return $value;
  }

  public function hincrby($key, $field, $value)
  {
    $this->redis->hincrby($this->redis_db_prefix . $key, $field, $value);
  }

  public function hdecrby($key, $field, $value)
  {
    $this->redis->hdecrby($this->redis_db_prefix . $key, $field, $value);
  }

  public function brpop($key, $field = 0)
  {
    $value = $this->redis->brpop($this->redis_db_prefix . $key, $field);
    return $value;
  }

  public function expireat($key, $timestamp = 0)
  {
    $value = $this->redis->EXPIREAT($this->redis_db_prefix . $key, $timestamp);
    return $value;
  }

  public function hdel($key, $field, $prefix = '')
  {
    if (empty($prefix)) {
      $this->redis->hdel($this->redis_db_prefix . $key, $field);
    } else {
      $this->redis->hdel($prefix . $key, $field);
    }
  }
  public function geoadd($key, $longitude, $latitude, $member, $prefix = '')
  {
    if (empty($prefix)) {
      $this->redis->geoadd($this->redis_db_prefix . $key, $longitude, $latitude, $member);
    } else {
      $this->redis->geoadd($prefix . $key, $longitude, $latitude, $member);
    }
  }
  public function georadius($key, $longitude, $latitude, $radius, $unit, $prefix = '')
  {
    if (empty($prefix)) {
      $this->redis->georadius($this->redis_db_prefix . $key, $longitude, $latitude, $radius, $unit, 'WITHDIST', 'ASC');
    } else {
      $this->redis->georadius($prefix . $key, $longitude, $latitude, $radius, $unit, 'WITHDIST', 'ASC');
    }
  }
}
