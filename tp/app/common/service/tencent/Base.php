<?php
/*
 * @Description    : 腾讯云
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2020-10-29 15:53:35
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-26 15:42:15
 */

namespace app\common\service\tencent;

class Base
{
    protected $config;
    public function __construct($config)
    {
        $this->config = $config;
    }
    public function curl_post($host, $params,$action,$service,$version)
    {
        $header = $this->makeHeader($host, $params,$action,$service,$version);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, 'https://' . $host);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt(
            $ch,
            CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    private function makeHeader($host, $params,$action,$service,$version)
    {
        $headers = [
            'X-TC-Action'    => $action,
            'X-TC-Timestamp' => time(),
            'X-TC-Version'   => $version,
            'Content-Type'   => 'application/json',
            'Host'           => $host
        ];
        $algo         = "TC3-HMAC-SHA256";
        $date         = gmdate("Y-m-d", $headers["X-TC-Timestamp"]);
        $canonicalUri = '/';
        $reqMethod    = 'POST';

        $canonicalQueryString = '';
        $canonicalHeaders     = "content-type:" . $headers["Content-Type"] . "\n" . "host:" . $headers["Host"] . "\n";
        $signedHeaders        = "content-type;host";
        $payloadHash          = hash("SHA256", json_encode($params));
        $canonicalRequest     = $reqMethod . "\n" . $canonicalUri . "\n" . $canonicalQueryString . "\n" . $canonicalHeaders . "\n" . $signedHeaders . "\n" . $payloadHash;

        $credentialScope        = $date . "/" . $service . "/tc3_request";
        $hashedCanonicalRequest = hash("SHA256", $canonicalRequest);
        $str2sign               = $algo . "\n" . $headers["X-TC-Timestamp"] . "\n" . $credentialScope . "\n" . $hashedCanonicalRequest;
        $signature              = self::signTC3($this->config['tenc_secret_key'], $date, $service, $str2sign);

        $auth                     = $algo . " Credential=" . $this->config['tenc_secret_id'] . "/" . $credentialScope . ", SignedHeaders=content-type;host, Signature=" . $signature;
        $headers["Authorization"] = $auth;

        $temp = [];
        foreach ($headers as $key => $value) {
            array_push($temp, $key . ':' . $value);
        }
        return $temp;
    }
    protected static function signTC3($skey, $date, $service, $str2sign)
    {
        $dateKey    = hash_hmac("SHA256", $date, "TC3" . $skey, true);
        $serviceKey = hash_hmac("SHA256", $service, $dateKey, true);
        $reqKey     = hash_hmac("SHA256", "tc3_request", $serviceKey, true);

        return hash_hmac("SHA256", $str2sign, $reqKey);
    }
}
