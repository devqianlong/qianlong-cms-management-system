<?php
/*
 * @Description    : 证书验证类
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-05 14:09:30
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-07 11:26:37
 */

namespace app\common\service\wxpay;
use app\lib\exception\ApiException;

/**
 * CertificateVerifier
 *
 * @category Class
 * @package  WechatPay
 * @author   WeChat Pay Team
 * @link     https://pay.weixin.qq.com
 */
class CertificateVerifier
{
    /**
     * WechatPay Certificates Public Keys
     *
     * @var array (serialNo => publicKey)
     */
    protected $publicKeys = [];

    /**
     * Constructor
     *
     * @param array of string|resource   $certifcates   WechatPay Certificates (string - PEM formatted \
     * certificate, or resource - X.509 certificate resource returned by openssl_x509_read)
     */
    public function __construct(array $certificates)
    {
        foreach ($certificates as $certificate) {
            $serialNo = PemUtil::parseCertificateSerialNo($certificate);
            $this->publicKeys[$serialNo] = \openssl_get_publickey($certificate);
        }
    }

    /**
     * Verify signature of message
     *
     * @param string $serialNumber  certificate serial number
     * @param string $message   message to verify
     * @param string $signautre signature of message
     *
     * @return bool
     */
    public function verify($serialNumber, $message, $signature)
    {
        $serialNumber = \strtoupper(\ltrim($serialNumber, '0')); // trim leading 0 and uppercase
        if (!isset($this->publicKeys[$serialNumber])) {
            return false;
        }
        if (!in_array('sha256WithRSAEncryption', \openssl_get_md_methods(true))) {
            throw new ApiException("当前PHP环境不支持SHA256withRSA");
        }
        $signature = \base64_decode($signature);
        return \openssl_verify($message, $signature, $this->publicKeys[$serialNumber], 
            'sha256WithRSAEncryption');
    }
}
