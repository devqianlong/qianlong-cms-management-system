<?php
/*
 * @Description    : 
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-03-29 23:35:46
 * @LastEditors    : QianLong
 * @LastEditTime   : 2022-06-09 16:17:18
 */

namespace app\index\controller;

use app\BaseController;
use app\common\requestPay;
use app\common\service\Alipay;
use app\common\service\Wxpay;

class Index extends BaseController
{
	public function alipay()
	{
		$order['out_trade_no'] = createId();
		$order['total_amount'] = '6000';
		$order['subject'] = '测试打款';
		$order['alipay'] = '15764138077';
		$order['name'] = '郭强';
		$result = requestPay::doPay($order['subject'],$order['total_amount'],$order['out_trade_no'], 'alipay_pc','','http://www.ecapi.cn');
		var_dump($result);
	}
	public function wx()
	{
		$order['out_trade_no'] = createId();
		$order['total_amount'] = '0.01';
		$order['subject'] = '测试支付';
		$url = 'http://face.shop.dragon.cn';
		$url = (new Wxpay)->payForJsapi($order, $url);
		return json($url);
	}
}
