<?php
/*
 * @Description    : 返回结果
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-08 13:06:53
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-04-16 16:58:29
 */

namespace app\lib;

class Result
{
    //success
    static public function Success($data=[],$msg='success')
    {
        $rs = [
            'code' => 200,
            'msg' => $msg,
            'data' => $data,
        ];
        return json($rs);
    }
    //error
    static public function Error($msg, $code = -1)
    {
        $rs = [
            'code' => $code,
            'msg' => $msg,
            'data' => '',
        ];
        return json($rs);
    }
}
