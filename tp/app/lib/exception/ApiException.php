<?php
/*
 * @Description    : 
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-08 21:46:30
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-11 13:11:04
 */

namespace app\lib\exception;


class ApiException extends BaseException
{
    public $code = -1;
    public $message = 'error';
    public function __construct($message,$code=-1)
    {
        $error['code'] = $code;
        if (!empty($message)) {
            $error['msg'] = $message;
        }
        parent::__construct($error);
    }
}