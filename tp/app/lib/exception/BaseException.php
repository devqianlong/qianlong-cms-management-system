<?php
/*
 * @Description    : 基础异常
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-08 21:43:05
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-11 13:11:18
 */

namespace app\lib\exception;


use think\Exception;
use Throwable;

class BaseException extends Exception
{
    //HTTP状态码
    public $code = 400;
    //错误具体信息
    public $message = '参数错误';
    //自定义错误码
    public $errCode = 10000;

    public function __construct($params = [])
    {
        if (!is_array($params)) {
            return;
        }
        if (array_key_exists('code', $params)) {
            $this->code = $params['code'];
        }
        if (array_key_exists('msg', $params)) {
            $this->message = $params['msg'];
        }
        if (array_key_exists('errCode', $params)) {
            $this->errCode = $params['errCode'];
        }
    }
}
