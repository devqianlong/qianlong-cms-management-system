<?php
/*
 * @Description    : 全局中间件定义文件
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-03-29 20:14:18
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-03-29 22:50:13
 */
return [
    \app\middleware\Cores::class
    // 全局请求缓存
    // \think\middleware\CheckRequestCache::class,
    // 多语言加载
    // \think\middleware\LoadLangPack::class,
    // Session初始化
    // \think\middleware\SessionInit::class
];
