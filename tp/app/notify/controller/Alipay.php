<?php
/*
 * @Description    : 支付宝支付异步通知接口
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-05 17:13:54
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-07 11:53:38
 */

namespace app\notify\controller;

use Alipay\EasySDK\Kernel\Factory;
use app\common\service\alipay\Options;

class Alipay extends \app\BaseController
{
    public function initialize()
    {
        Factory::setOptions((new Options)->getOptions());
    }
    public function url()
    {
        if (request()->isPost()) {
            $postData = input('param.');
            if (!empty($postData)) {
                $result = Factory::payment()->Common()->verifyNotify($postData);
                //支付成功，完成你的逻辑
                //例如连接数据库，获取付款金额$result['total_amount']，获取订单号$result['out_trade_no']修改数据库中的订单状态等;
                //交易金额，单位为元：$result['total_amount']
                //商户订单号：$result['out_trade_no']
                //支付宝交易号：$result['trade_no']
                //交易状态：$postData['trade_status']：TRADE_FINISHED（交易完结）、TRADE_SUCCESS（支付成功）
                //具体详细请看支付宝文档：https://opendocs.alipay.com/apis/api_1/alipay.trade.page.pay#%E5%93%8D%E5%BA%94%E5%8F%82%E6%95%B0
                var_dump($result);
                echo 'success';
            } else {
                echo "empty";
            }
        }
    }
}
