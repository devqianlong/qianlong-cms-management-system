<?php
use app\ExceptionHandle;
use app\Request;
use app\lib\exception\Http;

// 容器Provider定义文件
return [
    'think\Request'          => Request::class,
    'think\exception\Handle' => '\\app\\lib\\exception\\Http',
];
