<?php
/*
 * @Description    : 
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-03-29 20:14:18
 * @LastEditors    : QianLong
 * @LastEditTime   : 2023-07-11 14:03:57
 */
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用地址
    'app_host'         => env('app.host', ''),
    // 应用的命名空间
    'app_namespace'    => '',
    // 是否启用路由
    'with_route'       => false,
    // 默认应用
    'default_app'      => 'index',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => [],

    // 异常页面的模板文件
    'exception_tmpl'   => app()->getThinkPath() . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'    => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'   => true,

    'multi_login'   => true,

    // 状态
    'status' => [
        ['id' => 1, 'title' => '正常'],
        ['id' => -1, 'title' => '隐藏'],
    ],

    'mongodb' => [
        'type'    =>   '\think\mongo\Connection',
        'hostname'    =>  env('mongodb.host', ''),
        'database'    =>  env('mongodb.database', ''),
        'username'    =>  env('mongodb.username', ''),
        'password'     =>  env('mongodb.password', ''),
        'hostport'    =>   env('mongodb.port', ''),
    ],

    'SYS_STATIC' => [
        'CNZZ' => '<div style="display:none;"><script type="text/javascript" src="https://s23.cnzz.com/z_stat.php?id=1277711881&web_id=1277711881"></script></div>',
        'app_sign_auth_on' => true,
        'pass_salt' => 'BE427789F6A7195B798A5A9CC2E0CD5B',
        'login_expire' => 60 * 60,//单位：秒
        'login_refresh_expire' => 86400*2,//单位：秒
        'redis_expire' => 86400,//单位：秒
        'defaultAvatar' => 'https://img.alicdn.com/imgextra/i3/573804794/O1CN01bqcTDi1lHhGeFrJbM_!!573804794.jpg',
    ],
];
