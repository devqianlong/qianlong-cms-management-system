<?php
/*
 * @Description    : 
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-11-03 17:15:01
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-11-04 11:16:57
 */
// 中间件配置
return [
    // 别名或分组
    'alias'    => [],
    // 优先级设置，此数组中的中间件会按照数组中的顺序优先执行
    'priority' => [],
];
