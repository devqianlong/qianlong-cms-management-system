<?php
/*
 * @Description    : 应用入口文件
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-27 09:21:47
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-04-27 13:43:49
 */

// [ 应用入口文件 ]
namespace think;
define('UPLOAD_PATH', __DIR__.'/storage/');
require __DIR__ . '/../vendor/autoload.php';

// 执行HTTP应用并响应
$http = (new App())->http;

$response = $http->run();

$response->send();

$http->end($response);
