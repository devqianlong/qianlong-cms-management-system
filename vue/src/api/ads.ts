/*
 * @Description: 广告API接口
 * @Author: QianLong
 * @Date: 2019-07-07 19:06:52
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-04-28 09:46:08
 */
import http from '@/utils/http/axios'

export function getCateList(params) {
  return http.request({
    url: '/ads/cate_list',
    method: 'GET',
    params
  })
}
export function getAdsList(params) {
  return http.request({
    url: '/ads/list',
    method: 'GET',
    params
  })
}

export function delAdsItem(params) {
  return http.request(
    {
      url: '/ads/del_ads',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function delAdsCateItem(params) {
  return http.request(
    {
      url: '/ads/del_cate',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function editItemInfo(params) {
  return http.request(
    {
      url: '/ads/editItemInfo',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function editCateInfo(params) {
  return http.request(
    {
      url: '/ads/editCateItem',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function getCateWithLevel(params) {
  return http.request({
    url: '/ads/all_cate_list',
    method: 'GET',
    params
  })
}
export function getCateInfo(params) {
  return http.request({
    url: '/ads/editCateItem',
    method: 'GET',
    params
  })
}
export function getItemInfo(params) {
  return http.request({
    url: '/ads/editItemInfo',
    method: 'GET',
    params
  })
}
