/*
 * @Description: 分析页API接口
 * @Author: QianLong
 * @Date: 2019-07-07 19:06:52
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-10 10:34:36
 */
import http from '@/utils/http/axios'

export function getPageData(params) {
  return http.request({
    url: '/analysis/index',
    method: 'GET',
    params
  })
}
