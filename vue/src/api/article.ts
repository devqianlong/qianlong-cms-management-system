/*
 * @Description: 设置管理API接口
 * @Author: QianLong
 * @Date: 2019-07-07 19:06:52
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-04-27 11:46:17
 */
import http from '@/utils/http/axios'

export function getCateList(params) {
  return http.request({
    url: '/article/cate_list',
    method: 'GET',
    params
  })
}
export function getCateWithLevel(params) {
  return http.request({
    url: '/article/all_cate_list',
    method: 'GET',
    params
  })
}
export function getItemList(params) {
  return http.request({
    url: '/article/list',
    method: 'GET',
    params
  })
}

export function editData(params) {
  return http.request(
    {
      url: '/article/editItemInfo',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function getCateInfo(params) {
  return http.request({
    url: '/article/editCateItem',
    method: 'GET',
    params
  })
}

export function editCateInfo(params) {
  return http.request(
    {
      url: '/article/editCateItem',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function delArtItem(params) {
  return http.request(
    {
      url: '/article/delArt',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function delCateItem(params) {
  return http.request(
    {
      url: '/article/delCate',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function addData(params) {
  return http.request(
    {
      url: '/article/editItemInfo',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function getItemInfo(params) {
  return http.request({
    url: '/article/editItemInfo',
    method: 'GET',
    params
  })
}
