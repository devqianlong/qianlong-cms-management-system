/*
 * @Description: 通用API接口
 * @Author: QianLong
 * @Date: 2019-07-07 19:06:52
 * @LastEditors    : QianLong
 * @LastEditTime   : 2022-05-27 10:26:24
 */
import http from '@/utils/http/axios'

export function getLoginParam(params) {
  return http.request({
    url: '/sign/getLoginParam',
    method: 'GET',
    params
  })
}
