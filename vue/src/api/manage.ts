/*
 * @Description: 设置管理API接口
 * @Author: QianLong
 * @Date: 2019-07-07 19:06:52
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-04-30 11:32:06
 */
import http from '@/utils/http/axios'

export function getUserList(params) {
  return http.request({
    url: '/user/getUserList',
    method: 'GET',
    params
  })
}

export function getAuthGroup(params) {
  return http.request({
    url: '/user/getAuthGroup',
    method: 'GET',
    params
  })
}

export function getMenuWithLevel(params) {
  return http.request({
    url: '/menu/getMenuWithLevel',
    method: 'GET',
    params
  })
}

export function doMenuEdit(params) {
  return http.request(
    {
      url: '/menu/menu_edit',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function doMenuDel(params) {
  return http.request(
    {
      url: '/menu/menu_del',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function getMenuList(params) {
  return http.request({
    url: '/menu/auth_menu',
    method: 'GET',
    params
  })
}

export function getRoleList(params) {
  return http.request({
    url: '/roles/index',
    method: 'GET',
    params
  })
}

export function getRoleInfo(params) {
  return http.request({
    url: '/roles/editAuthGroup',
    method: 'GET',
    params
  })
}

export function editData(params) {
  return http.request(
    {
      url: '/roles/editAuthGroup',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function addData(params) {
  return http.request(
    {
      url: '/roles/addAuthGroup',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function delUser(params) {
  return http.request(
    {
      url: '/user/delUser',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function editUserInfo(params) {
  return http.request({
    url: '/user/editUserInfo',
    method: 'GET',
    params
  })
}

export function postUserInfo(params) {
  return http.request(
    {
      url: '/user/editUserInfo',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function addUserInfo(params) {
  return http.request(
    {
      url: '/user/addUserInfo',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function getUserInfo(params) {
  return http.request({
    url: '/roles/editAuthGroup',
    method: 'GET',
    params
  })
}

export function forbidItem(params) {
  return http.request(
    {
      url: '/roles/forbidAuthGroup',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function delItem(params) {
  return http.request(
    {
      url: '/roles/delAuthGroup',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function editparams(params) {
  return http.request(
    {
      url: '/roles/editAuthGroup',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function getSysSettingInfo(params) {
  return http.request({
    url: '/sys_setting/editItemInfo',
    method: 'GET',
    params
  })
}

export function editSysSettingparams(params) {
  return http.request(
    {
      url: '/sys_setting/editItemInfo',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function addparams(params) {
  return http.request(
    {
      url: '/roles/addAuthGroup',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
