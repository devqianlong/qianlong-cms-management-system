/*
 * @Description: 个人设置API接口
 * @Author: QianLong
 * @Date: 2019-07-07 19:06:52
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-05-31 14:40:37
 */
import http from '@/utils/http/axios'

export function getSecuritySetting(params) {
  return http.request({
    url: '/personal/security_setting',
    method: 'GET',
    params
  })
}

export function getBindingAccount(params) {
  return http.request({
    url: '/personal/binding_account',
    method: 'GET',
    params
  })
}

export function changePwd(params) {
  return http.request(
    {
      url: '/personal/change_pwd',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function bindOauth(params) {
  return http.request(
    {
      url: '/personal/bind_oauth',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function changePhone(params) {
  return http.request(
    {
      url: '/personal/change_phone',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
