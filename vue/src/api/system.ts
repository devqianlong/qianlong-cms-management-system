/*
 * @Description: 系统设置API接口
 * @Author: QianLong
 * @Date: 2019-07-07 19:06:52
 * @LastEditors    : QianLong
 * @LastEditTime   : 2023-07-07 17:17:37
 */
import http from '@/utils/http/axios'

export function getPaySetting(params) {
  return http.request({
    url: '/system/pay_setting',
    method: 'GET',
    params
  })
}

export function editPaySetting(params) {
  return http.request(
    {
      url: '/system/pay_setting',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function getSmsCode(params) {
  return http.request(
    {
      url: '/common/getSmsCode',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function getSmsSetting(params) {
  return http.request({
    url: '/system/sms_setting',
    method: 'GET',
    params
  })
}

export function editSmsSetting(params) {
  return http.request(
    {
      url: '/system/sms_setting',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function getPushSetting(params) {
  return http.request({
    url: '/system/push_setting',
    method: 'GET',
    params
  })
}

export function editPushSetting(params) {
  return http.request(
    {
      url: '/system/push_setting',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function getSysSetting(params) {
  return http.request({
    url: '/system/sys_setting',
    method: 'GET',
    params
  })
}

export function editSysSetting(params) {
  return http.request(
    {
      url: '/system/sys_setting',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function getOssSetting(params) {
  return http.request({
    url: '/system/oss_setting',
    method: 'GET',
    params
  })
}

export function editOssSetting(params) {
  return http.request(
    {
      url: '/system/oss_setting',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function getEmailSetting(params) {
  return http.request({
    url: '/system/email_setting',
    method: 'GET',
    params
  })
}

export function editEmailSetting(params) {
  return http.request(
    {
      url: '/system/email_setting',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
export function testEmailSetting(params) {
  return http.request(
    {
      url: '/system/email_test',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
