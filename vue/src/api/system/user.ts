import http from '@/utils/http/axios'
import { LoginParams } from './model/userModel'

enum Api {
  login = '/sign/login',
  forgot_password = '/sign/doforgotpwd',
  register = '/sign/doRegister',
  refresh = '/user/refresh_token',
  logout = '/sign/logout'
}

/**
 * @description: 获取用户信息
 */
export function getUserInfo() {
  return http.request(
    {
      url: Api.login,
      method: 'POST'
    },
    {
      isTransformRequestResult: false
    }
  )
}
/**
 * @description: 刷新token
 */
export function refreshToken() {
  return http.request(
    {
      url: Api.refresh,
      method: 'GET'
    },
    {
      isTransformRequestResult: false
    }
  )
}

/**
 * @description: 用户登录
 */
export function login(params: LoginParams) {
  return http.request(
    {
      url: Api.login,
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
/**
 * @description: 用户锁屏登录
 */
export function lock_login(params: LoginParams) {
  return http.request(
    {
      url: '/personal/lock_login',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
/**
 * @description: 用户找回密码
 */
export function forgotPassword(params) {
  return http.request(
    {
      url: Api.forgot_password,
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}
/**
 * @description: 用户注册
 */
export function doRegister(params) {
  return http.request(
    {
      url: Api.register,
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

/**
 * @description: 用户修改密码
 */
export function changePassword(params, uid) {
  return http.request(
    {
      url: `/user/u${uid}/changepw`,
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

export function getSmsCaptcha(params) {
  return http.request(
    {
      url: '/login/getSmsCode',
      method: 'POST',
      params
    },
    {
      isTransformRequestResult: false
    }
  )
}

/**
 * @description: 用户登出
 */
export function logout(params) {
  return http.request({
    url: Api.logout,
    method: 'POST',
    params
  })
}
