import { ObjectDirective } from 'vue'
import { hasPermission } from '@/utils/permission/hasPermission'

export const permission: ObjectDirective = {
  // mounted(el: HTMLButtonElement, binding, vnode) {
  mounted(el: HTMLElement, binding, vnode) {
    if (binding.value == undefined) return

    const { action, effect } = binding.value
    // 如果action不传，则认为不需要授权认证
    if (action == undefined) {
      return
    }
    if (!hasPermission(action)) {
      ;(el.parentNode && el.parentNode.removeChild(el)) || (el.style.display = 'none')
    }
  }
}
