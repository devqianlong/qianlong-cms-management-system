import { defineComponent } from 'vue'

import { Layout, Divider } from 'ant-design-vue'
import { CopyrightOutlined } from '@ant-design/icons-vue'
import styles from './index.module.scss'

const { Footer: ALayoutFooter } = Layout

export default defineComponent({
  name: 'PageFooter',
  components: { ALayoutFooter, [Divider.name]: Divider },
  setup() {
    return () => (
      <>
        <a-layout-footer class={styles.page_footer}>
          <div class={styles.copyright}>
            Copyright <CopyrightOutlined /> 2021-现在
            <a-divider type="vertical" />
            <a href="http://21ds.cn" target="_blank">
              千龙提供技术支持
            </a>
            <a-divider type="vertical" />
            <a href="http://ecapi.cn" title="专业的电商cps Api接口平台" target="_blank">
              喵有券API平台
            </a>
            <a-divider type="vertical" />
            <a href="http://www.21ds.cn" title="企业开发平台" target="_blank">
              企业开发者平台（IDP）
            </a>
            <a-divider type="vertical" />
            <a href="http://ecmpay.cn" title="专业的聚合支付平台" target="_blank">
              喵付通
            </a>
            <a-divider type="vertical" />
            <a href="http://writer.doc.21ds.cn" title="Markdown文档托管平台" target="_blank">
              文档托管
            </a>
          </div>
        </a-layout-footer>
      </>
    )
  }
})
