/*
 * @Description    : main
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-22 16:48:38
 * @LastEditors    : QianLong
 * @LastEditTime   : 2023-07-11 15:12:42
 */
// import './publicPath'
import { createApp } from 'vue'
import App from './App.vue'
import router, { setupRouter } from './router'
import { setupStore } from '@/store'
// import './assets/iconfont/iconfont.css'
import dayjs from 'dayjs'
import 'dayjs/locale/zh-cn'
dayjs.locale('zh-cn')
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)
// utcOffset 偏移八小时
dayjs().utcOffset(8)
import { setupAntd, setupDirectives, setupGlobalMethods, setupCustomComponents } from '@/plugins'
const app = createApp(App)

// 注册全局常用的ant-design-vue组件
setupAntd(app)
// 注册全局自定义组件
setupCustomComponents(app)
// 注册全局自定义指令，如：v-permission权限指令
setupDirectives(app)
// 注册全局方法，如：app.config.globalProperties.$message = message
setupGlobalMethods(app)
// 挂载vuex状态管理
setupStore(app)
// 挂载路由
setupRouter(app)
// 路由准备就绪后挂载APP实例
router.isReady().then(() => app.mount('#app'))
