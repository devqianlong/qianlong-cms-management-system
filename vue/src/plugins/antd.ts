/*
 * @Description    : antd.ts
 * @Author         : QianLong
 * @Date           : 2023-02-18 14:26:49
 * @LastEditors    : QianLong
 * @Site           : http://www.21ds.cn
 * @LastEditTime   : 2023-07-05 14:40:42
 */
import type { App } from 'vue'

import {
  Modal,
  Button,
  Table,
  Menu,
  Input,
  Form,
  Card,
  Checkbox,
  Radio,
  Tabs,
  Tooltip,
  Dropdown,
  Tag,
  Select,
  List,
  Steps,
  Alert
} from 'ant-design-vue'

import 'ant-design-vue/dist/reset.css'

export function setupAntd(app: App<Element>) {
  app
    .use(Form)
    .use(Input)
    .use(Modal)
    .use(Table)
    .use(Menu)
    .use(Card)
    .use(Checkbox)
    .use(Radio)
    .use(Button)
    .use(Tabs)
    .use(Tooltip)
    .use(Dropdown)
    .use(Tag)
    .use(Select)
    .use(List)
    .use(Steps)
    .use(Alert)
}
