/*
 * @Description    : route
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-22 16:48:38
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-04-30 09:50:40
 */
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import { createRouterGuards } from './router-guards'
import 'nprogress/css/nprogress.css' // 进度条样式

import constantRouter from './modules/constantRouter'
import common from '@/router/common'
import { App } from 'vue'

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Layout',
    redirect: '/dashboard/Workplace',
    component: () => import(/* webpackChunkName: "layout" */ '@/layout/index.vue'),
    meta: {
      title: '首页'
    },
    children: [...common]
  },
  ...constantRouter
]

const router = createRouter({
  // process.env.BASE_URL
  history: createWebHistory(),
  routes
})

export function setupRouter(app: App) {
  app.use(router)
  // 创建路由守卫
  createRouterGuards(router)
}
export default router
