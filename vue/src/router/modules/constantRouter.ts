import { RouteRecordRaw } from 'vue-router'

/**
 * 不需要授权就可以访问的页面
 */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'login',
    redirect: '/login',
    component: () => import(/* webpackChunkName: "layout" */ '@/layout/LoginReg.vue'),
    meta: {
      title: '登录/注册/找回密码'
    },
    children: [
      {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "login" */ '@/views/user/Login.vue')
      },
      {
        path: '/register',
        name: 'register',
        component: () => import(/* webpackChunkName: "login" */ '@/views/user/Register.vue')
      },
      {
        path: '/forgot-password',
        name: 'ForgotPassword',
        component: () => import(/* webpackChunkName: "login" */ '@/views/user/ForgotPassword.vue')
      }
    ]
  }
]

export default routes
