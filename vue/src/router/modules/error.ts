/*
 * @Description    : error
 * @Author         : QianLong
 * @Date           : 2023-02-18 14:26:49
 * @LastEditors    : QianLong
 * @Site           : http://www.21ds.cn
 * @LastEditTime   : 2023-07-05 11:00:22
 */
import { RouterTransition } from '@/components/transition'

const routeName = 'error'

export const notFound = {
  path: '/:pathMatch(.*)*',
  name: 'NotFound',
  redirect: '/error/404',
  children: [],
  component: () => import(/* webpackChunkName: "404" */ '@/views/extend/error/404.vue')
}

export const errorRoutes = {
  path: '/error',
  name: routeName,
  redirect: '/error/404',
  component: RouterTransition,
  meta: {
    title: '错误页',
    icon: 'EditOutlined',
    hidden: true
  },
  children: [
    {
      path: '404',
      name: `${routeName}-404`,
      meta: {
        title: '404',
        icon: 'UserOutlined'
      },
      component: () => import(/* webpackChunkName: "404" */ '@/views/extend/error/404.vue')
    }
  ]
}
