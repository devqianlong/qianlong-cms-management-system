import { ActionContext } from 'vuex'
import { IUserState } from './state'
import { getUserInfo, login, lock_login, refreshToken } from '@/api/system/user'
import { ACCESS_TOKEN, CURRENT_USER, IS_LOCKSCREEN, LOGIN_EXPIRE } from '@/store/mutation-types'
import { storage } from '@/utils/Storage'
import store from '@/store'
import { IStore } from '@/store/types'

export const actions = {
  // 登录
  async login({ commit }: ActionContext<IUserState, IStore>, userInfo) {
    try {
      const response = await login(userInfo)
      const { data, code } = response
      if (code == 200) {
        storage.set(ACCESS_TOKEN, data.userToken, data.Expire)
        storage.set(CURRENT_USER, data, data.Expire)
        storage.set(LOGIN_EXPIRE, data.loginExpire, data.Expire)
        storage.set(IS_LOCKSCREEN, false)
        commit('setToken', data.userToken)
        commit('setLoginExpire', data.loginExpire)
        commit('setRoles', data.rules)
        // todo
        commit('setUserInfo', data)
        store.commit('lockscreen/setLock', false)
      }
      return Promise.resolve(response)
    } catch (e) {
      return Promise.reject(e)
    }
  },
  // 刷新token
  async refresh({ commit }: ActionContext<IUserState, IStore>) {
    try {
      const response = await refreshToken()
      const { data, code } = response
      if (code == 200) {
        storage.set(ACCESS_TOKEN, data.userToken, data.Expire)
        storage.set(CURRENT_USER, data, data.Expire)
        storage.set(LOGIN_EXPIRE, data.loginExpire, data.Expire)
        storage.set(IS_LOCKSCREEN, false)
        commit('setToken', data.userToken)
        commit('setRoles', data.rules)
        commit('setLoginExpire', data.loginExpire)
        // todo
        commit('setUserInfo', data)
        store.commit('lockscreen/setLock', false)
      }
      return Promise.resolve(response)
    } catch (e) {
      return Promise.reject(e)
    }
  },
  // 锁屏登录
  async lock_login({ commit }: ActionContext<IUserState, IStore>, userInfo) {
    try {
      const response = await lock_login(userInfo)
      const { code } = response
      if (code == 200) {
        storage.set(IS_LOCKSCREEN, false)
        // todo
        store.commit('lockscreen/setLock', false)
      }
      return Promise.resolve(response)
    } catch (e) {
      return Promise.reject(e)
    }
  },

  // 获取用户信息
  getUserInfo({ commit }: ActionContext<IUserState, IStore>) {
    return new Promise((resolve, reject) => {
      getUserInfo()
        .then((response) => {
          const result = response.data
          if (result.role && result.role.permissions.length > 0) {
            const role = result.role
            role.permissions = result.role.permissions
            role.permissions.map((per) => {
              if (per.actionEntitySet != null && per.actionEntitySet.length > 0) {
                const action = per.actionEntitySet.map((action) => {
                  return action.action
                })
                per.actionList = action
              }
            })
            role.permissionList = role.permissions.map((permission) => {
              return permission.permissionId
            })
            commit('setRoles', result.role)
            commit('setUserInfo', result)
          } else {
            reject(new Error('getInfo: roles must be a non-null array !'))
          }

          // commit('SET_NAME', { name: result.name, welcome: welcome() })
          commit('setAvatar', result.avatar)

          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  // 登出
  async logout({ commit }: ActionContext<IUserState, IStore>) {
    commit('setRoles', [])
    commit('setUserInfo', '')
    storage.remove(ACCESS_TOKEN)
    storage.remove(CURRENT_USER)
    return Promise.resolve('')
  }
}
