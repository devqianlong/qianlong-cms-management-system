import { createStorage } from '@/utils/Storage'
import { ACCESS_TOKEN, CURRENT_USER, LOGIN_EXPIRE } from '@/store/mutation-types'
const Storage = createStorage({ storage: localStorage })

export type IUserState = {
  token: string
  loginExpire: number
  name: string
  welcome: string
  avatar: string
  roles: any[]
  info: any
}

export const state: IUserState = {
  token: Storage.get(ACCESS_TOKEN, ''),
  loginExpire: Storage.get(LOGIN_EXPIRE, 0),
  name: '',
  welcome: '',
  avatar: '',
  roles: [],
  info: Storage.get(CURRENT_USER, {})
}
