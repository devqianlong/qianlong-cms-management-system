/*
 * @Description    : 通用反馈
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-21 23:34:08
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-04-23 12:17:14
 */
import { notification } from 'ant-design-vue'

export function requestSuccess(res, description = '操作成功', message = '提示') {
  if (res === null) {
    notification.success({
      message: message,
      description: description,
      duration: 4
    })
    return false
  }
  if (res.code === 200) {
    notification.success({
      message: message,
      description: res.msg,
      duration: 5
    })
  } else {
    requestFailed(res)
  }
}

export function requestFailed(res, description = '操作失败', message = '错误') {
  if (res === null) {
    notification.error({
      message: message,
      description: description,
      duration: 2
    })
    return false
  }
  notification.error({
    message: message,
    description: res.msg,
    duration: 2
  })
}
