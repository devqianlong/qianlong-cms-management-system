// axios配置  可自行根据项目进行更改，只需更改该文件即可，其他文件可以不动

import { VAxios } from './Axios'
import { AxiosTransform } from './axiosTransform'
import axios from 'axios'
import qs from 'qs'
import { checkStatus } from './checkStatus'
import { Modal, message as Message } from 'ant-design-vue'
import { RequestEnum, ResultEnum, ContentTypeEnum } from '@/enums/httpEnum'

import { isString } from '@/utils/is/index'
import { setObjToUrlParams } from '@/utils/urlUtils'

import { RequestOptions } from './types'

import router from '@/router'
import store from '@/store'
import { storage } from '@/utils/Storage'
import { apiUrl } from '@/utils/common'
/**
 * @description: 数据处理，方便区分多种处理方式
 */
const transform: AxiosTransform = {
  /**
   * @description: 处理请求数据
   */
  transformRequestData: (res, options: RequestOptions) => {
    const {
      isTransformRequestResult,
      isShowMessage = true,
      isShowErrorMessage,
      isShowSuccessMessage,
      successMessageText,
      errorMessageText
    } = options

    const reject = Promise.reject
    const { config, data } = res
    if (data.code == -2) {
      Message.error(data.msg || '登录已过期，请重新登录')
      router.replace({
        name: 'login',
        query: {
          redirect: router.currentRoute.value.fullPath
        }
      })
      storage.clear()
      return reject(new Error(data.msg))
    }
    if (data.code == -401) {
      Message.error(data.msg || '访问过于频繁，已被强制退出，请重新登录，并注意访问频率')
      router.replace({
        name: 'login',
        query: {
          redirect: router.currentRoute.value.fullPath
        }
      })
      storage.clear()
      return reject(new Error(data.msg))
    }
    if (data.code === -99) {
      // 防止过度刷新请求
      const times = storage.get('refresh_times')
      const refresh_times = times ? parseInt(times) : 0
      if (refresh_times <= 10) {
        storage.set('refresh_times', refresh_times + 1, 5 * 60 * 1000)
      } else {
        // 到登录页
        const timeoutMsg = '获取必要信息过于频繁,请重新登录!'
        Message.error(timeoutMsg || '操作失败！')
        storage.clear()
        router.replace({
          name: 'login',
          query: {
            redirect: router.currentRoute.value.fullPath
          }
        })
        return false
      }
      store
        .dispatch('user/refresh')
        .then(async (res2) => {
          if (res2.code == 200) {
            // eslint-disable-next-line
            config.headers.Authorization = 'Bear ' + res2.data.userToken + ':' + new Date().getTime()
            return Axios.request(config)
          }
        })
        .catch((err) => {
          console.log(err)
        })
      return
      // return reject()
    }
    // 请求成功
    const hasSuccess = data && Reflect.has(data, 'code') && data.code === ResultEnum.SUCCESS
    // 是否显示提示信息
    if (isShowMessage) {
      if (hasSuccess && (successMessageText || isShowSuccessMessage)) {
        // 是否显示自定义信息提示
        Message.success(successMessageText || data.msg || '操作成功！')
      } else if (!hasSuccess && (errorMessageText || isShowErrorMessage)) {
        // 是否显示自定义信息提示
        Message.error(data.msg || errorMessageText || '操作失败！')
      } else if (!hasSuccess && options.errorMessageMode === 'modal') {
        // errorMessageMode=‘custom-modal’的时候会显示modal错误弹窗，而不是消息提示，用于一些比较重要的错误
        Modal.confirm({ title: '错误提示', content: data.msg })
      }
    }
    // 不进行任何处理，直接返回
    // 用于页面代码可能需要直接获取code，data，message这些信息时开启
    if (!isTransformRequestResult) {
      return res.data
    }

    if (!data) {
      // return '[HTTP] Request has no return value';
      return reject(data)
    }

    // 接口请求成功，直接返回结果
    if (data.code === ResultEnum.SUCCESS) {
      return data.data
    }
    // 接口请求错误，统一提示错误信息
    if (data.code === ResultEnum.ERROR) {
      if (data.msg) {
        Message.error(data.msg)
        reject(new Error(data.msg))
      } else {
        const msg = '操作失败,系统异常!'
        Message.error(msg)
        reject(new Error(msg))
      }
      return reject()
    }

    // 登录超时
    if (data.code === ResultEnum.TIMEOUT) {
      if (router.currentRoute.value.name == 'login') return
      // 到登录页
      const timeoutMsg = '登录超时,请重新登录!'
      Modal.destroyAll()
      Modal.warning({
        title: '提示',
        content: '登录身份已失效,请重新登录!',
        onOk: () => {
          router.replace({
            name: 'login',
            query: {
              redirect: router.currentRoute.value.fullPath
            }
          })
          storage.clear()
        }
      })
      return reject(new Error(timeoutMsg))
    }

    // 这里逻辑可以根据项目进行修改
    if (!hasSuccess) {
      return reject(new Error(data.msg))
    }
    return data
  },

  // 请求之前处理config
  beforeRequestHook: (config, options) => {
    const { joinParamsToUrl, isParseToJson } = options

    if (config.url?.indexOf('http://') == -1 && config.url?.indexOf('https://') == -1) {
      config.url = apiUrl() + `api${config.url}`
    }

    if (config.method === RequestEnum.GET) {
      const now = new Date().getTime()
      if (!isString(config.params)) {
        config.data = {
          // 给 get 请求加上时间戳参数，避免从缓存中拿数据。
          params: Object.assign(config.params || {}, {
            _t: now
          })
        }
      } else {
        // 兼容restful风格
        config.url = config.url + config.params + `?_t=${now}`
        config.params = {}
      }
    } else {
      if (!isString(config.params)) {
        config.data = config.params
        config.params = {}
        if (joinParamsToUrl) {
          config.url = setObjToUrlParams(config.url as string, config.data)
        }
      } else {
        // 兼容restful风格
        config.url = config.url + config.params
        config.params = {}
      }
      // 'a[]=b&a[]=c'
      if (!isParseToJson) {
        // eslint-disable-next-line
        config.params = !isString(config.params) ? qs.stringify(config.params, { arrayFormat: 'brackets' }) : config.params
        // eslint-disable-next-line
        config.data = !isString(config.data) ? qs.stringify(config.data, { arrayFormat: 'brackets' }) : config.data
      }
    }
    return config
  },

  /**
   * @description: 请求拦截器处理
   */
  requestInterceptors: (config) => {
    // 请求之前处理config
    const token = store.state.user.token
    if (token) {
      // jwt token
      config.headers.Authorization = 'Bear ' + token + ':' + new Date().getTime()
    }
    return config
  },

  /**
   * @description: 响应错误处理
   */
  responseInterceptorsCatch: (error: any) => {
    const { response, code, message } = error || {}
    const msg: string =
      response && response.data && response.data.error ? response.data.error.message : ''
    const err: string = error.toString()
    try {
      if (code === 'ECONNABORTED' && message.indexOf('timeout') !== -1) {
        Message.error('接口请求超时,请刷新页面重试!')
        return
      }
      if (err && err.includes('Network Error')) {
        Modal.confirm({
          title: '网络异常',
          content: '请检查您的网络连接或服务器程序是否正常!'
        })
        return
      }
    } catch (error) {
      throw new Error('Error:' + error)
    }
    // 请求是否被取消
    const isCancel = axios.isCancel(error)
    if (!isCancel) {
      checkStatus(error.response && error.response.status, msg)
    } else {
      console.warn(error, '请求被取消！')
    }
    return error
  }
}

const Axios = new VAxios({
  timeout: 15 * 1000,
  // 基础接口地址
  // baseURL: globSetting.apiUrl,
  // 接口可能会有通用的地址部分，可以统一抽取出来
  // prefixUrl: prefix,
  headers: { 'Content-Type': ContentTypeEnum.FORM_URLENCODED },
  // 数据处理方式
  transform,
  // 配置项，下面的选项都可以在独立的接口请求中覆盖
  requestOptions: {
    // 默认将prefix 添加到url
    joinPrefix: true,
    // 需要对返回数据进行处理
    isTransformRequestResult: true,
    // post请求的时候添加参数到url
    joinParamsToUrl: false,
    // 格式化提交参数时间
    formatDate: true,
    // 消息提示类型
    errorMessageMode: 'none',
    // 接口地址
    apiUrl: apiUrl()
  },
  withCredentials: false
})
export default Axios
