/*
 * @Description    : 验证js
 * @Version        : 1.0.0
 * @Author         : QianLong
 * @Date           : 2021-04-21 16:46:07
 * @LastEditors    : QianLong
 * @LastEditTime   : 2021-06-21 15:08:35
 */
/* 是否手机号码 */
export function validatePhone(rules, value) {
  const reg = /^[1][3,4,5,7,8,9][0-9]{9}$/
  if (value == '' || value == undefined || value == null) {
    return Promise.resolve()
  } else {
    if (!reg.test(value) && value != '') {
      return Promise.reject('请输入正确的手机号码')
    } else {
      return Promise.resolve()
    }
  }
}
/* 是否验证码 */
export function validateCaptcha(rules, value) {
  if (value.length < 4 || value.length > 6) {
    return Promise.reject('请输入正确的验证码')
  } else {
    return Promise.resolve()
  }
}

/* 是否邮箱 */
export function validateEMail(rules, value) {
  /* eslint-disable-next-line */
  const reg = /^([a-zA-Z0-9]+[-_\.]?)+@[a-zA-Z0-9]+\.[a-z]+$/
  if (value == '' || value == undefined || value == null) {
    return Promise.resolve()
  } else {
    if (!reg.test(value)) {
      return Promise.reject('请输入正确的邮箱地址')
    } else {
      return Promise.resolve()
    }
  }
}

// 验证密码
export function validatePassword(rules, value) {
  // eslint-disable-next-line prettier/prettier
  const reg = /^([A-Z]|[a-z]|[0-9]|[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>\/?~！@#￥%……&*（）――+|{}【】‘；：”“'。，、？]){8,20}$/
  if (value == '' || value == undefined || value == null) {
    return Promise.resolve()
  } else {
    if (!reg.test(value)) {
      return Promise.reject('请输入8-20位的密码')
    } else {
      return Promise.resolve()
    }
  }
}

// 验证用户名
export function validateUserName(rules, value) {
  const reg = /^([A-Z]|[a-z]|[0-9]){5,12}$/
  if (value == '' || value == undefined || value == null) {
    return Promise.resolve()
  } else {
    if (!reg.test(value)) {
      return Promise.reject('用户名仅支持英文及数字组合，5~12个字符')
    } else {
      return Promise.resolve()
    }
  }
}

// 验证身份证号
export function validateIdCard(rules, value) {
  var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
  if (value == '' || value == undefined || value == null) {
    return Promise.resolve()
  } else {
    if (!reg.test(value)) {
      return Promise.reject('请输入正确的身份证号')
    } else {
      return Promise.resolve()
    }
  }
}

// 是否是固定号码
export function validateFixPhone(rules, value) {
  const reg = /^([0-9]{3,4}-)?[0-9]{7,8}$/
  if (value == '' || value == undefined || value == null) {
    return Promise.resolve()
  } else {
    if (!reg.test(value)) {
      return Promise.reject('请输入正确的固定电话')
    } else {
      return Promise.resolve()
    }
  }
}

//验证两位小数
export function validateTwoFloat(rules, value) {
  const reg = /^(\d+|\d+\.\d{1,2})$/
  if (value == '' || value == undefined || value == null) {
    return Promise.resolve()
  } else {
    if (!reg.test(value)) {
      return Promise.reject('请输入正确两位小数')
    } else {
      return Promise.resolve()
    }
  }
}

//验证整数
export function validateZhengshu(rules, value) {
  const reg = /^[1-9]\d*$/
  if (value == '' || value == undefined || value == null) {
    return Promise.resolve()
  } else {
    if (!reg.test(value)) {
      return Promise.reject('请输入正整数')
    } else {
      return Promise.resolve()
    }
  }
}
